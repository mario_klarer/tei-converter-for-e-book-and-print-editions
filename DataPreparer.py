import glob
import os
import csv
import xml.etree.ElementTree as elTree
from Helpers import Helpers
from Model.Textregion import Textregion
import json
from shutil import copyfile


class DataPreparer:

    def __init__(self, folder, output_folder, csv_file):
        self.output_xml_directory = output_folder
        self.xmlfiles = glob.glob('./' + folder + '/page/*.xml')
        self.imgfiles = glob.glob('./' + folder + '/*.jpg')
        self.xmlfiles = sorted(self.xmlfiles)
        self.imgfiles = sorted(self.imgfiles)
        self.folder = folder
        self.csv_data = {}

        self.csv_file = csv_file

        first = True
        with open(csv_file) as csv_file_buffer:
            csv_reader = csv.reader(csv_file_buffer, delimiter=';')
            for row in csv_reader:
                if first is True:
                    first = False
                    continue

                append = {'name': row[0], 'image_start': row[3], 'image_end': row[4], 'strophes': row[5],
                          'band': row[6], 'absolute': row[7] == "true"}

                if append['band'] in self.csv_data:
                    self.csv_data[append['band']].append(append)
                else:
                    self.csv_data[append['band']] = [append]

    def prepare_data(self):
        keys = self.csv_data.keys()
        for band in keys:
            for i, csv_row in enumerate(self.csv_data[band]):
                current_img_files = []
                current_xml_files = []
                add = False
                start_adding_lines = False
                end_adding_lines = False

                ti_start_name = "00000000" + csv_row['image_start']
                ti_start_name = ti_start_name[-8:]
                ti_end_name = "00000000" + csv_row['image_end']
                ti_end_name = ti_end_name[-8:]

                for index, imgfile in enumerate(self.imgfiles):
                    if ti_start_name in imgfile:
                        add = True

                    if add is True:
                        current_img_files.append(imgfile)
                        current_xml_files.append(self.xmlfiles[index])

                    if ti_end_name in imgfile:
                        break

                for xml_file_index, xmlfile in enumerate(current_xml_files):
                    print("Preparing " + xmlfile + " for output...")
                    xmlroot = elTree.parse(xmlfile).getroot()
                    ns = {'d': Helpers.get_namespace()}

                    page = xmlroot.find('d:Page', ns)

                    regions = page.findall('d:TextRegion', ns)

                    added_lines = 0

                    root = elTree.Element('PcGts')
                    root.set('xmlns', Helpers.get_namespace())
                    new_page = elTree.SubElement(root, 'Page')

                    for ind, region in enumerate(regions):
                        textregion = Textregion(region)
                        textregion_reading_order = int(textregion.get_options()['readingOrder'][0]['index'])

                        if csv_row['absolute'] is False and textregion_reading_order > 2:
                            continue
                        lines = textregion.get_lines()

                        textregion_options = textregion.get_options()
                        textregion = elTree.SubElement(new_page, 'TextRegion')

                        add_empty_lines = 0
                        lines_skipped = 0

                        for index, line in enumerate(lines):
                            line_options = line.get_options()

                            if csv_row['absolute'] is False:
                                if 'startText' in line_options and start_adding_lines is True:
                                    end_adding_lines = True

                                if 'startText' in line_options and start_adding_lines is False:
                                    start_adding_lines = True
                                    end_adding_lines = False

                                if start_adding_lines is False:
                                    add_empty_lines += 1
                                    if line.get_junicode() != "":
                                        lines_skipped += 1
                                    continue
                                elif end_adding_lines is True:
                                    continue

                            if add_empty_lines > 0:
                                for add_line_index in range(add_empty_lines):
                                    added_lines += 1

                                    empty_line = lines[index - add_empty_lines + add_line_index]

                                    xml_line = elTree.SubElement(textregion, 'TextLine')
                                    coords = elTree.SubElement(xml_line, 'Coords')
                                    coordinates = empty_line.get_coords()
                                    coordstring = Helpers.create_coordstring(coordinates, 1, [0, 0])
                                    coords.set('points', coordstring)
                                    baseline = empty_line.get_baseline()
                                    bline = elTree.SubElement(xml_line, 'Baseline')
                                    baselinestring = Helpers.create_coordstring(baseline, 1, [0, 0])
                                    bline.set('points', baselinestring)
                                    junicode = elTree.SubElement(elTree.SubElement(xml_line, 'TextEquiv'), 'Unicode')
                                    junicode.text = ''
                                    xml_line.set('custom', json.dumps({'readingOrder': empty_line.get_options()['readingOrder']}))
                                    xml_line.set('readingOrder', str(empty_line.get_reading_order()))

                            added_lines += 1

                            xml_line = elTree.SubElement(textregion, 'TextLine')
                            coords = elTree.SubElement(xml_line, 'Coords')
                            coordinates = line.get_coords()
                            coordstring = Helpers.create_coordstring(coordinates, 1, [0,0])
                            coords.set('points', coordstring)
                            baseline = line.get_baseline()
                            bline = elTree.SubElement(xml_line, 'Baseline')
                            baselinestring = Helpers.create_coordstring(baseline, 1, [0,0])
                            bline.set('points', baselinestring)
                            junicode = elTree.SubElement(elTree.SubElement(xml_line, 'TextEquiv'), 'Unicode')
                            junicode.text = line.get_junicode()
                            opts = line.get_options()
                            if add_empty_lines != 0:
                                opts['emptyLine'] = [{'offset': 0, 'length': 0, 'value': add_empty_lines}]
                            xml_line.set('custom', json.dumps(opts))
                            xml_line.set('readingOrder', str(line.get_reading_order()))

                            add_empty_lines = 0

                        if added_lines == 0:
                            textregion_options['readingOrder'][0]['index'] = 999

                        if lines_skipped != 0:
                            textregion.set('leftOffset', str(lines_skipped))

                        textregion.set('custom', json.dumps(textregion_options))

                    tree = elTree.ElementTree(root)

                    xml_output_path = self.output_xml_directory + "/" + band + "/" + str(i) + '_' + csv_row['name'] + "/page"
                    img_output_path = self.output_xml_directory + "/" + band + "/" + str(i) + '_' + csv_row['name']

                    if not os.path.exists(img_output_path):
                        os.makedirs(img_output_path)

                    if not os.path.exists(xml_output_path):
                        os.makedirs(xml_output_path)

                    copyfile(current_img_files[xml_file_index], img_output_path + "/" + os.path.basename(current_img_files[xml_file_index]))

                    tree.write(
                        xml_output_path + "/" + os.path.basename(xmlfile))

        return True

