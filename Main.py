from glob import glob
import os
from shutil import copyfile

from ImageCropper import ImageCropper
from PdfGenerator import PdfGenerator
from PdfCombiner import PdfCombiner
from DataPreparer import DataPreparer

test = DataPreparer('src/full_ti', 'src/TI', 'src/data/texts.csv')

test.prepare_data()

subdirs_band = glob("src/TI/*/")

for j, subdir_band in enumerate(subdirs_band):
    band = os.path.basename(os.path.normpath(subdir_band))

    subdirs = sorted(glob("src/TI/" + band + "/*/"))

    if band != "1":
        continue

    current_offset = 0

    for i, subdir in enumerate(subdirs):
        column_title = os.path.basename(os.path.normpath(subdir))[2:]

        folder_prepend = os.path.basename(os.path.normpath(subdir))[:2]

        test = ImageCropper("src/TI/" + band + "/" + folder_prepend + column_title, column_title, '',
                            '', '')

        test.cropimages()

        del test

        test = PdfGenerator("output_cropper/" + column_title, column_title.upper(), '', False, '', '',
                            '', '', current_offset, column_title, i == 0)

        current_offset = test.generate_pdfs() - 1

        del test

        test = PdfCombiner("output_pdf/" + column_title)

        test.combine_pdfs()

        if not os.path.exists("final_output/" + band):
            os.makedirs("final_output/" + band)

        copyfile("output_pdf/" + column_title + '/full_output.pdf', "final_output/" + band + "/" + str(i) + '_' + column_title + ".pdf")

        del test

for subdir_band in subdirs_band:
    band = os.path.basename(os.path.normpath(subdir_band))

    if band != "1":
        continue

    test = PdfCombiner("final_output/" + band)

    test.combine_pdfs()

    del test