from Helpers import Helpers


class Line:

    def __init__(self, options, coords, baseline, junicode, reading_order, set_cesuras):
        self.__options = options
        self.__coords = coords
        self.__baseline = baseline
        self.__junicode = junicode
        self.__reading_order = reading_order
        self.__junicode_formatted = (junicode + '.')[:-1]

        capital_letters = Helpers.get_capital_letter_array()
        capital_letter_replacements = Helpers.get_capital_letter_replacement_array()
        capital_letter_red_replacements = Helpers.get_capital_letter_replacement_red_array()
        capital_letter_blue_replacements = Helpers.get_capital_letter_replacement_blue_array()

        bold_letter_replacement_options = ['bigInitial', 'redInitial', 'blueInitial', 'missingInitial']
        replacements = [capital_letter_replacements, capital_letter_red_replacements, capital_letter_blue_replacements, capital_letter_replacements]

        has_replacement = False

        for j, opt in enumerate(bold_letter_replacement_options):
            if opt in options:
                has_replacement = True
                for initial in options[opt]:
                    if 'offset' in initial:
                        offset = int(initial['offset'])
                        replacement_index = -1
                        for index, letter in enumerate(capital_letters):
                            if self.__junicode[offset] == letter:
                                replacement_index = index
                                break

                        if replacement_index != -1:
                            self.__junicode = self.__junicode[:offset] + replacements[j][
                                replacement_index] + self.__junicode[offset + 1:]
                            self.__junicode_formatted = self.__junicode_formatted[:offset] + replacements[j][
                                replacement_index] + self.__junicode_formatted[offset + 1:]

        if 'folio' in options and 'is_folio' in options['folio'][0] and options['folio'][0]['is_folio'] == 'true':
            self.__is_folio = True
        else:
            self.__is_folio = False

        self.__strikethroughs = []
        if 'textStyle' in options:
            additional_offset = 0
            for strike in options['textStyle']:
                if 'strikethrough' in strike and strike['strikethrough'] == 'true' and 'offset' in strike and 'length' in strike:
                    offset_normal = int(strike['offset'])
                    offset = offset_normal + additional_offset
                    length = int(strike['length'])
                    additional_offset += 17

                    self.__strikethroughs.append(self.__junicode[offset:offset + length])

                    self.__junicode_formatted = self.__junicode_formatted[
                                                :offset] + "<strike>" + self.__junicode_formatted[
                                                                        offset:offset + length] + "</strike>" + self.__junicode_formatted[
                                                                                                                (offset + length):]

        if set_cesuras is True and 'caesura' in options:
            for caesura in options['caesura']:
                if 'offset' in caesura:
                    offset = int(caesura['offset'])
                    self.__junicode = self.__junicode[:offset] + "ϴ" + self.__junicode[offset + 1:]


        self.__insertions = []
        if 'insertion' in options:
            for insertion in options['insertion']:
                self.__insertions.append(insertion['insert'])

        self.__abbreviations = []
        if 'abbrev' in options:
            for abbrev in options['abbrev']:
                if 'expansion' in abbrev and 'offset' in abbrev and 'length' in abbrev:
                    abbreviated = self.__junicode[
                                  int(abbrev['offset']):(int(abbrev['offset'])
                                                         + int(
                                              abbrev['length']))]

                    if has_replacement:
                        abbrev['expansion'] = abbrev['expansion'].replace(abbrev['expansion'][0], abbreviated[0])

                    self.__abbreviations.append({'expansion': abbrev['expansion'],
                                                 'abbreviated': abbreviated})
        try:
            self.__empty_lines = int(options['emptyLine'][0]['value'])
        except (IndexError, KeyError):
            self.__empty_lines = 0

    def get_options(self):
        return self.__options

    def get_coords(self):
        return self.__coords

    def get_baseline(self):
        return self.__baseline

    def get_junicode(self):
        return self.__junicode

    def get_junicode_formatted(self):
        return self.__junicode_formatted

    def get_junicode_normalized(self):
        return Helpers.normalize_string(self.__junicode)

    def get_reading_order(self):
        return self.__reading_order

    def get_abbreviations(self):
        return self.__abbreviations

    def get_is_folio(self):
        return self.__is_folio

    def get_insertions(self):
        return self.__insertions

    def get_empty_lines(self):
        return self.__empty_lines

    def get_strikethroughs(self):
        return self.__strikethroughs

    def is_incipit(self):
        return 'incipit' in self.__options or 'explicit' in self.__options

    def is_end_of_verse_contained(self):
        return "endOfVerse" in self.__options

    def is_end_of_verse_on_line_end(self):
        if self.is_end_of_verse_contained() is False:
            return False

        last_verse_key = self.get_verse_amount() - 1

        return len(self.__junicode) == (int(self.__options['endOfVerse'][last_verse_key]['offset'])
                                        + int(self.__options['endOfVerse'][last_verse_key]['length']))

    def get_verse_amount(self):
        if self.is_end_of_verse_contained():
            return len(self.__options['endOfVerse'])
        else:
            return 0

    def get_verse_string(self, from_tag):

        verse_number = ""
        strophe_number = ""

        if from_tag < -1:
            from_tag = -1

        is_incipit = 'incipit' in self.__options or 'explicit' in self.__options
        aventiure = -1

        text_start = 'startText' in self.__options

        line_junicode = self.__junicode

        if "amen" in self.__options:
            amen_offset = int(self.__options['amen'][0]['offset'])
            amen_length = int(self.__options['amen'][0]['length'])

            for amen_index in range(amen_offset, amen_offset + amen_length):
                line_junicode = line_junicode[:amen_index] + ' ' + line_junicode[amen_index + 1:]

        if is_incipit:
            if 'startAventiure' in self.__options:
                aventiure = self.__options['startAventiure'][0]['value']
            return line_junicode + " ", True, True, True, is_incipit, verse_number, strophe_number, aventiure

        if not self.is_end_of_verse_contained():
            if 'startAventiure' in self.__options:
                aventiure = self.__options['startAventiure'][0]['value']
            return line_junicode + " ", False, False, text_start, is_incipit, verse_number, strophe_number, aventiure

        verse_tags = self.__options['endOfVerse']

        if from_tag == -1:
            start_from = 0
        else:
            start_from = int(verse_tags[from_tag]["offset"]) + int(verse_tags[from_tag]["length"])

        try:
            end_at = int(verse_tags[from_tag + 1]["offset"]) + int(verse_tags[from_tag + 1]["length"])
            verse_end = True
            if 'value' in verse_tags[from_tag + 1]:
                verse_number = verse_tags[from_tag + 1]["value"]
            else:
                verse_number = "MVT"
        except IndexError:
            end_at = len(self.__junicode)
            verse_end = False

        if 'startAventiure' in self.__options and int(self.__options['startAventiure'][0]['offset']) >= start_from and int(self.__options['startAventiure'][0]['offset']) < end_at:
            aventiure = self.__options['startAventiure'][0]['value']

        ret = ""

        for i in range(start_from, end_at):
            if i < len(self.__junicode):
                ret += line_junicode[i]

        if end_at == len(self.__junicode) and start_from != end_at:
            ret += " "

        strophe_end = False
        if 'endOfStrophe' in self.__options:
            strophe_tag = self.__options['endOfStrophe'][0]
            if 'value' in strophe_tag:
                strophe_number = strophe_tag['value']
            else:
                strophe_number = "MVT"
            strophe_end = (int(strophe_tag['offset']) + int(strophe_tag['length'])) == end_at

        return ret, verse_end, strophe_end, text_start, is_incipit, verse_number, strophe_number, aventiure
