from Helpers import Helpers
from Model.Line import Line
import json
import os
import sys

class Textregion:

    def __init__(self, region, options_json=False, set_caesura=False):
        ns = {'d': Helpers.get_namespace()}
        if region.get('custom') is not None:
            if options_json is False:
                self.__options = Helpers.parse_options(region)
            else:
                self.__options = json.loads(region.get('custom'))
        else:
            self.__options = {}

        if region.get('leftOffset') is not None:
            self.__left_offset = int(region.get('leftOffset'))
        else:
            self.__left_offset = 0

        if region.get('chapter') is not None:
            self.__chapter = region.get('chapter')
        else:
            self.__chapter = "TEI"

        if region.get('rightOffset') is not None:
            self.__right_offset = int(region.get('rightOffset'))
        else:
            self.__right_offset = 0

        self.__has_amen = False

        self.__lines = []
        xml_lines = region.findall('d:TextLine', ns)
        for line in xml_lines:
            if options_json is False:
                line_options = Helpers.parse_options(line)
                line_reading_order = int(line_options['readingOrder'][0]['index'])
            else:
                line_options = json.loads(line.get('custom'))
                line_reading_order = line.get('readingOrder')

            if "amen" in line_options:
                self.__has_amen = True

            line_coords = {"max_x": 0, "max_y": 0, "min_x": 0, "min_y": 0}
            if line.find('d:Coords', ns) is not None:
                line_coords = Helpers.parse_coordinates(line.find('d:Coords', ns).get('points'))
            line_baseline = {"max_x": 0, "max_y": 0, "min_x": 0, "min_y": 0}
            if line.find('d:Baseline', ns) is not None:
                line_baseline = Helpers.parse_baseline(line.find('d:Baseline', ns).get('points'))
            line_text_equiv = line.find('d:TextEquiv', ns)
            if line_text_equiv is not None:
                line_junicode = line_text_equiv.find('d:Unicode', ns).text
                line_junicode = "" if line_junicode is None else line_junicode
                self.__lines.append(
                    Line(options=line_options, coords=line_coords, baseline=line_baseline, junicode=line_junicode,
                         reading_order=line_reading_order, set_cesuras=set_caesura))

    def get_options(self):
        return self.__options

    def get_right_offset(self):
        return self.__right_offset

    def get_left_offset(self):
        return self.__left_offset

    def get_chapter(self):
        return self.__chapter

    def get_lines(self):
        return self.__lines

    def get_has_amen(self):
        return self.__has_amen

    def get_lines_without_empty(self):
        return list(filter(lambda x: x.get_junicode() != "", self.__lines))

    def get_lines_by_reading_order(self, start, end):
        return list(filter(lambda item: (start <= int(item.get_reading_order()) <= end), self.__lines))

    def has_strophes(self):
        lines = self.get_lines()
        has_strophes = False

        for line in lines:
            line_options = line.get_options()
            if 'endOfStrophe' in line_options:
                has_strophes = True

        return has_strophes

    def get_first_strophe_number(self):
        if self.has_strophes() is False:
            return ""

        lines = self.get_lines()

        try:
            for line in lines:
                line_options = line.get_options()
                if 'endOfStrophe' in line_options:
                    return bytes(line_options['endOfStrophe'][0]['value'], 'utf-8').decode('unicode-escape')
        except IndexError:
            print("First Strophe Parse Error")

        return ""

    def last_verse_lines(self):
        index = -1
        lines = self.get_lines()
        ret = []
        try:
            if lines[index].is_incipit():
                while index > -20:
                    ret.append(lines[index])
                    if lines[index-1].is_incipit() is False:
                        return ret[::-1]
                    index -= 1
            else:
                if lines[index].is_end_of_verse_on_line_end():
                    return ret
                while index > -20:
                    ret.append(lines[index])
                    if lines[index].is_end_of_verse_contained():
                        return ret[::-1]
                    index -= 1
        except (IndexError, KeyError):
            return ret[::-1]

    def first_verse_lines(self):
        index = 0
        lines = self.get_lines()
        ret = []
        strophe_num = 0
        verse_num = 0
        try:
            if 'startText' in lines[index].get_options():
                return ret, verse_num, strophe_num
            while index < 20:
                ret.append(lines[index])
                if lines[index].is_incipit():
                    if lines[index+1].is_incipit() is False:
                        return ret, verse_num, strophe_num
                else:
                    if lines[index].is_end_of_verse_contained():
                        opts = lines[index].get_options()

                        if "endOfStrophe" in opts:
                            if 'value' in opts['endOfStrophe'][0]:
                                strophe_num = opts['endOfStrophe'][0]['value']
                            else:
                                strophe_num = 1
                        else:
                            smallest_verse = 9999999
                            for opt in opts['endOfVerse']:
                                if 'value' in opt and Helpers.represents_int(opt['value']):
                                    smallest_verse = int(opt['value']) if int(opt['value']) < smallest_verse else smallest_verse

                            verse_num = str(smallest_verse)
                        return ret, verse_num, strophe_num
                index += 1
        except:
            return ret, verse_num, strophe_num
