<h1>TEI Converter for E-Book and Print Editions</h1>

<h2>Metadata</h2>

<ul>
<li>
<b>Project title:</b> TEI Converter for E-Book and Print Editions
</li>

<li>
<b>Principal investigator:</b> Univ.-Prof. Dr. Mario Klarer, University of Innsbruck (https://orcid.org/0000-0003-0712-9328)
</li>

<li>
<b>Funding organization:</b> Austrian Academy of Sciences (ÖAW), Austrian Centre for Digital Humanities and Cultural Heritage (ACDH-CH)
</li>

<li>
<b>Project website:</b> https://www.uibk.ac.at/projects/ahb/tei-converter/
</li>

<li>
<b>License:</b> MIT License
</li>
</ul>

<h2>Required Packages</h2>
<ul>
<li>
Pillow: 7.0.0
</li>
<li>
PyPDF2: 1.26.0
</li>
<li>
pip: 20.0.2
</li>
<li>
reportlab: 3.5.34
</li>
<li>
setuptools: 45.2.0
</li>
</ul>
