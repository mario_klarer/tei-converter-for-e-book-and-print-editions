import sys
from PyPDF2 import PdfFileReader, PdfFileWriter
import glob
import os


class PdfCombiner:

    def __init__(self, folder, sort_by_name=False):
        self.xmlfiles = glob.glob('./' + folder + '/*.pdf')
        if sort_by_name is False:
            self.xmlfiles.sort(key=os.path.getmtime)
        else:
            self.xmlfiles.sort(key=os.path.basename)
        self.folder = folder

    def combine_pdfs(self):
        pdf_writer = PdfFileWriter()

        for path in self.xmlfiles:
            pdf_reader = PdfFileReader(path)
            for page in range(pdf_reader.getNumPages()):
                pdf_writer.addPage(pdf_reader.getPage(page))

        with open('./' + self.folder + '/full_output.pdf', 'wb') as fh:
            pdf_writer.write(fh)
