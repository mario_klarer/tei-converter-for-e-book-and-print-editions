import glob
import xml.etree.ElementTree as elTree
import os, sys
from Helpers import Helpers
from Model.Textregion import Textregion
from PIL import Image as pilimage
import csv

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.enums import TA_JUSTIFY, TA_RIGHT, TA_LEFT, TA_CENTER
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, PageBreak, Frame, \
    PageTemplate, NextPageTemplate, Table, TableStyle, Spacer
from reportlab.lib.colors import pink, black, red, blue, green
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib import colors
from functools import reduce


class PdfGenerator:
    column_title_first_page_addition = ""

    font_size = 11
    font_size_initial = 20
    font_size_h1 = 16
    font_size_big_initial = 51
    font_size_h2 = 13
    font_size_additives = 9
    font_size_legend = font_size_additives
    font_size_verse_numbers = font_size_additives

    is_right_side_line_height_static = False
    is_cmyk = True

    margin_right = 15  # mm
    margin_left = 15  # mm
    margin_top = 15  # mm
    margin_bottom = 10  # mm

    margin_image_left = margin_right  # mm
    margin_image_right = margin_left  # mm
    margin_image_vertical = margin_top + font_size  # mm

    canvas_size = (Helpers.mm_to_pts(210), Helpers.mm_to_pts(280))

    table_row_height = 13.5

    set_strophe_number = True
    strophe_counter = 1
    verse_counter = 0
    strophe_integer = 1
    page_number = 13
    has_strophes = False
    spacing_between_table_columns = 12
    superscript_buffer = 10

    is_drawing_left = False

    line_scale = 0.5

    image_spacer_y = 0
    image_verse_number_space = 5  # mm
    image_caption_space = 3  # mm

    header_text_page_top_space = 15.5  # mm 23
    header_text_line_space = 2  # mm
    header_line_verses_space = 12  # mm

    max_height_lines = 635

    header_line_outside_offset = 0

    __current_csv_row = {}

    is_intro = False

    def __init__(self, folder, column_title="TEI", csv_file='', absolute_positioning = False, big_initial_csv='', row_height_csv='', offset_file = '', skip_aventiure_file = '', page_number_offset = 0, first_page_title="", skip_first_empty=False):
        self.xmlfiles = glob.glob('./' + folder + '/xml/*.xml')
        self.imgfiles = glob.glob('./' + folder + '/img/*.jpg')
        self.xmlfiles.sort(key=os.path.getmtime)
        self.imgfiles.sort(key=os.path.getmtime)
        self.__current_lines = []
        self.__image_width = 0
        self.current_chapter = ""
        self.column_title = column_title
        self.folder = folder
        self.page_number_offset = page_number_offset
        self.page_number = self.page_number + self.page_number_offset
        self.skip_first_empty = skip_first_empty

        if first_page_title == "":
            self.first_page_title = column_title
        else:
            self.first_page_title = first_page_title

        self.__row_count = {'left': 0, 'right': 0}
        self.__max_string_width = {'left': 0, 'right': 0, 'left_number': 0, 'right_number': 0, 'fol_left': '',
                                   'fol_right': '', 'line_left': '', 'line_right': '', 'line_right_index': 0,
                                   'error_pages': [], }
        self.__current_file = ''

        self.csv_file = csv_file
        self.big_initial_csv = big_initial_csv
        self.row_height_csv = row_height_csv
        self.offset_file = offset_file
        self.skip_aventiure_file = skip_aventiure_file
        self.offset_data = []
        self.skip_aventiures_data = []
        self.csv_data = {}
        self.big_initial_data = {}
        self.row_height_data = {}
        self.absolute_positioning = absolute_positioning

        if self.row_height_csv != '':
            first = True
            with open(self.row_height_csv) as row_height_csv_file_buffer:
                csv_reader = csv.reader(row_height_csv_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    append = {'count':row[0], 'height':row[1]}

                    self.row_height_data[row[0]] = append

        if self.skip_aventiure_file != '':
            first = True
            with open(self.skip_aventiure_file) as skip_aventiures_file_buffer:
                csv_reader = csv.reader(skip_aventiures_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    self.skip_aventiures_data.append(row[0])

        if offset_file != '':
            first = True
            with open(offset_file) as offset_file_buffer:
                csv_reader = csv.reader(offset_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    offset_lines = list(map(lambda x: int(x), row[3].split(',')))
                    lines_insert = list(map(lambda x: int(x), row[4].split(',')))

                    if lines_insert[0] == 0:
                        lines_insert = lines_insert[1:]
                        offset_lines = offset_lines[1:]

                    offset_lines_list = []

                    for offset_index, offset in enumerate(lines_insert):
                        offset_lines_list.append({'line-insert': offset, 'line':offset_lines[offset_index], })

                    append = {'file': row[0], 'y': int(row[1]), 'x': int(row[2]), 'lines': offset_lines_list, }
                    self.offset_data.append(append)

        if self.big_initial_csv != '':
            first = True
            with open(self.big_initial_csv) as big_initial_csv_file_buffer:
                csv_reader = csv.reader(big_initial_csv_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    append = {'fol': row[0], 'letter': row[1], 'place_at': row[2], 'skip_lines':  row[3].split(',')}

                    self.big_initial_data[row[0] + '.jpg'] = append

        if self.csv_file != '':
            first = True
            with open(self.csv_file) as csv_file_buffer:
                csv_reader = csv.reader(csv_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    append = {'image': row[0], 'x': row[1], 'y': row[2], 'width': row[3], 'height': row[4],
                              'caption': row[5], 'textregion': row[6], 'linebeginning': row[7], 'lineending': row[8],
                              'fol': row[9].split(';'), 'connect': row[10], }

                    self.csv_data[row[5] + '.jpg'] = append

    def generate_pdfs(self):
        pdfmetrics.registerFont(TTFont('Junicode', './fonts/Junicode-reportlab.ttf'))
        pdfmetrics.registerFont(TTFont('JunicodeBd', './fonts/Junicode-Bold-reportlab.ttf'))
        pdfmetrics.registerFont(TTFont('JunicodeBI', './fonts/Junicode-BoldItalic.ttf'))
        pdfmetrics.registerFont(TTFont('JunicodeIt', './fonts/Junicode-Italic.ttf'))

        previous_textregion = None

        style_sheet = getSampleStyleSheet()
        style_sheet.add(
            ParagraphStyle(name='Junicode', fontName='Junicode', fontSize=self.font_size, leading=12,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_JUSTIFY, ))

        style_sheet.add(
            ParagraphStyle(name='JunicodeBigInitial', fontName='Junicode', fontSize=self.font_size_big_initial,
                           leading=23, textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_JUSTIFY, ))

        style_sheet.add(
            ParagraphStyle(name='JunicodeRight', fontName='Junicode', fontSize=self.font_size, leading=12,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_RIGHT, ))

        style_sheet.add(
            ParagraphStyle(name='JunicodeRightSmall', fontName='Junicode', fontSize=self.font_size_additives, leading=12,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_RIGHT, ))

        style_sheet.add(
            ParagraphStyle(name='JunicodeBd', fontName='JunicodeBd', fontSize=self.font_size, leading=12,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_JUSTIFY, ))
        style_sheet.add(
            ParagraphStyle(name='H1', fontName='Junicode', fontSize=self.font_size_h1, leading=self.font_size_h1 + 10,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_JUSTIFY, ))
        style_sheet.add(
            ParagraphStyle(name='H2', fontName='Junicode', fontSize=self.font_size_h2, leading=self.font_size_h2 + 8,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_JUSTIFY, ))
        style_sheet.add(
            ParagraphStyle(name='Standart', fontName='Junicode', fontSize=self.font_size_additives, leading=12,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_JUSTIFY))
        style_sheet.add(
            ParagraphStyle(name='Center', fontName='Junicode', fontSize=self.font_size, leading=12,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_CENTER))
        style_sheet.add(
            ParagraphStyle(name='Junicode-Center', fontName='Junicode', fontSize=self.font_size, leading=12,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           alignment=TA_CENTER))
        style_sheet.add(
            ParagraphStyle(name='Junicode-Center-Letter', fontName='Junicode', fontSize=self.font_size_h1,
                           textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                           leading=self.font_size_h1,
                           alignment=TA_CENTER))

        if not os.path.exists('output_pdf/' + self.column_title):
            os.makedirs('output_pdf/' + self.column_title)

        initial_document = SimpleDocTemplate('output_pdf/' + self.column_title + '/initial.pdf',
                                             pagesize=self.canvas_size,
                                             rightMargin=Helpers.mm_to_pts(self.margin_right),
                                             leftMargin=Helpers.mm_to_pts(self.margin_left),
                                             topMargin=Helpers.mm_to_pts(self.margin_top),
                                             bottomMargin=Helpers.mm_to_pts(self.margin_bottom))

        initial_frame = Frame(initial_document.leftMargin, initial_document.bottomMargin, initial_document.width,
                              initial_document.height)

        init_text_template = PageTemplate(id='init_textpage', frames=[initial_frame], onPage=self.__draw_init_tables)
        empty_page_template = PageTemplate(id='empty', frames=[initial_frame])
        init_template = PageTemplate(id='initial', frames=[initial_frame], onPage=self.__draw_init)

        if self.is_intro is True:
            initial_document.addPageTemplates([init_text_template, init_template, ])

        init_page_flow = []

        table_column_space = self.canvas_size[0] - Helpers.mm_to_pts(self.margin_left) - Helpers.mm_to_pts(
            self.margin_right)

        init_page_flow.append(NextPageTemplate('initial'), )
        init_page_flow.append(PageBreak(), )
        init_page_flow.append(Spacer(1, 1), )

        if self.is_intro is True:
            initial_document.build(init_page_flow)
        else:
            if self.skip_first_empty:
                initial_document.addPageTemplates([init_template, ])
                init_page_flow = [Spacer(1, 1), PageBreak()]
                initial_document.build(init_page_flow)
                self.page_number = 2 + self.page_number_offset
            else:
                initial_document.addPageTemplates([empty_page_template, init_template, ])
                init_page_flow = [Spacer(1, 1), ]
                init_page_flow.append(NextPageTemplate('initial'), )
                init_page_flow.append(PageBreak())
                init_page_flow.append(Spacer(1, 1))
                initial_document.build(init_page_flow)
                self.page_number = 3 + self.page_number_offset

        start_with_zero_pivot = False
        previous_ended_strophe = False
        start_of_text = True
        indent_lines = 0
        for index, xmlfile in enumerate(self.xmlfiles):
            self.__current_file = xmlfile
            ns = {'d': Helpers.get_namespace()}

            suffix_verse_num = 0
            suffix_strophe_num = 0

            imgfile = ""
            for file in self.imgfiles:
                if os.path.splitext(os.path.basename(file))[0] == os.path.splitext(os.path.basename(xmlfile))[0]:
                    imgfile = file

            if imgfile == "":
                continue

            xmlroot = elTree.parse(xmlfile).getroot()

            page = xmlroot.find('d:Page', ns)

            xml_region = page.find('d:TextRegion', ns)

            region = Textregion(xml_region, True, True)

            self.is_drawing_left = "drawing" in region.get_options() and int(
                region.get_options()['drawing'][0]['left']) == 1

            self.current_chapter = region.get_chapter()

            if not os.path.exists('output_pdf/' + self.column_title):
                os.makedirs('output_pdf/' + self.column_title)

            document = SimpleDocTemplate('output_pdf/' + self.column_title + '/' + os.path.splitext(os.path.basename(imgfile))[0] + ".pdf",
                                         pagesize=self.canvas_size,
                                         rightMargin=Helpers.mm_to_pts(self.margin_right),
                                         leftMargin=Helpers.mm_to_pts(self.margin_left),
                                         topMargin=Helpers.mm_to_pts(self.margin_top),
                                         bottomMargin=Helpers.mm_to_pts(self.margin_bottom))

            image = pilimage.open(imgfile)
            img_width, img_height = image.size

            pdf_image_height = img_height / 300 * 72
            pdf_image_width = pdf_image_height * img_width / img_height
            pdf_image = Image(imgfile, pdf_image_width, pdf_image_height)

            self.__image_width = pdf_image_width

            document_image = SimpleDocTemplate(
                'output_pdf/' + self.column_title + '/' + os.path.splitext(os.path.basename(imgfile))[0] + ".pdf",
                pagesize=self.canvas_size,
                rightMargin=Helpers.mm_to_pts(self.margin_image_right),
                leftMargin=Helpers.mm_to_pts(self.margin_image_left),
                topMargin=Helpers.mm_to_pts(self.margin_image_vertical),
                bottomMargin=0)

            frame = Frame(document.leftMargin, document.bottomMargin, document.width, document.height)

            frame_image = Frame(document_image.leftMargin, document_image.bottomMargin, document_image.width,
                                document_image.height)

            region_lines = region.get_lines()

            if self.absolute_positioning:
                self.__current_lines = []

                if self.csv_file != '':
                    if os.path.basename(imgfile) in self.csv_data:
                        self.__current_csv_row = self.csv_data[os.path.basename(imgfile)]
                    else:
                        continue

                for j, line in enumerate(region_lines):
                    relative_offset_image = pdf_image_height / img_height * \
                                        (line.get_baseline()[
                                             'min_y'] - 30)
                    self.__current_lines.append({"line": line, "relative_offset_image": relative_offset_image})

                template = PageTemplate(id='imagepage', frames=[frame_image], onPage=self.__draw_static_absolute)
                text_template = PageTemplate(id='textpage', frames=[frame], onPage=self.__draw_text_page_absolute)

                document.addPageTemplates([
                                            template,
                                            text_template,
                                           ])

                page_flow = [NextPageTemplate('imagepage'), Spacer(0, self.image_spacer_y), pdf_image,
                             Spacer(0, (self.image_caption_space - 1) / 25.4 * 72),
                             Paragraph(os.path.splitext(os.path.basename(imgfile))[0], style=style_sheet["Center"]),
                             NextPageTemplate('textpage'), PageBreak(), PageBreak(),
                             ]

                try:
                    document.build(page_flow)
                except:
                    print('Could not build Page' + xmlfile)

                self.page_number += 2

                continue

            skip_right_pivot = False
            try:
                try:
                    if previous_textregion is not None:
                        last_lines = previous_textregion.last_verse_lines()
                        if last_lines is not None:
                            prefix_verse = self.__parse_verse_lines(last_lines)[-1]
                        else:
                            prefix_verse = None
                    else:
                        prefix_verse = None
                except IndexError:
                    prefix_verse = None

                try:
                    nextxmlroot = elTree.parse(self.xmlfiles[index + 1]).getroot()

                    nextpage = nextxmlroot.find('d:Page', ns)

                    xml_nextregion = nextpage.find('d:TextRegion', ns)

                    next_textregion = Textregion(xml_nextregion, True, True)

                    next_textregion_first_strophe = next_textregion.get_first_strophe_number()

                    first_lines, first_lines_verse_num, first_lines_strophe = next_textregion.first_verse_lines()

                    suffix_verse = None

                    if first_lines is not None:
                        suffix_verse = self.__parse_verse_lines(first_lines)[0]

                        suffix_verse_num = first_lines_verse_num
                        suffix_strophe_num = first_lines_strophe
                except:
                    next_textregion_first_strophe = ""
                    suffix_verse = None

                previous_textregion = region

                self.has_strophes = region.has_strophes()

                if self.has_strophes is False:
                    self.strophe_counter = 1
                    self.set_strophe_number = True

                junicode_line_table_data = []

                empty_lines = 0
                big_initial = ""

                if self.has_strophes is False:
                    table_column_space_numerations_1 = 11.0
                    table_column_space_numerations_2 = 60.5
                    table_text_column_space_first = 201.4 + 5
                else:
                    table_column_space_numerations_1 = 11.0
                    table_column_space_numerations_2 = 0
                    table_text_column_space_first = 0

                    for j, line in enumerate(region.get_lines()):
                        str_len_to_append = stringWidth(Helpers.replace_superscripts_capital_letters(Helpers.replace_helper_chars(line.get_junicode_formatted().replace('<strike>', '').replace('</strike>', ''))), "Junicode", self.font_size)

                        if str_len_to_append > table_text_column_space_first:
                            table_text_column_space_first = str_len_to_append

                    table_text_column_space_first = table_text_column_space_first + 5

                    for j, verse in enumerate(self.__parse_verse_lines(region.get_lines())):
                        try:
                            if verse['strophe_end']:
                                strophe_number = bytes(verse['strophe_number'], 'utf-8').decode('unicode-escape')

                                str_len_verse_num_text_pre = stringWidth(strophe_number, "Junicode", self.font_size)

                                if str_len_verse_num_text_pre > table_column_space_numerations_2:
                                    table_column_space_numerations_2 = str_len_verse_num_text_pre
                        except:
                            print("Value Error at strophe")

                    table_column_space_numerations_2 = table_column_space_numerations_2 + Helpers.mm_to_pts(5)

                table_text_column_space_second = table_column_space - table_text_column_space_first - table_column_space_numerations_1 - table_column_space_numerations_2 - 2*self.spacing_between_table_columns + self.superscript_buffer

                big_initial_indent = 45

                max_y = 0

                for line in region_lines:
                    curr_y = line.get_baseline()["min_y"]
                    if curr_y > max_y:
                        max_y = curr_y

                average_spacing = ((pdf_image_height / img_height * max_y) - (
                        pdf_image_height / img_height * region_lines[0].get_baseline()["min_y"])) / (
                                          len(region_lines) - 1) if len(region_lines) - 1 != 0 else 0

                counting_leading_empty_lines = True
                leading_empty_lines = 0
                pivot_left_alignment_positions = []
                pivot_left_strophe_numbers = []
                if start_with_zero_pivot:
                    try:
                        first_pivot_line = region.get_lines()[0]

                        if first_pivot_line.get_junicode_formatted() != "":
                            is_append_left_pivot = True
                        else:
                            is_append_left_pivot = False
                    except:
                        is_append_left_pivot = False
                    if is_append_left_pivot:
                        pivot_left_alignment_positions.append(0)
                    start_with_zero_pivot = False
                pivot_right_alignment_positions = []
                last_pivot_was_incipit = False
                is_count_leading_empty = True
                for j, line in enumerate(region.get_lines()):
                    empty_lines_iteration = line.get_empty_lines()
                    empty_lines += empty_lines_iteration

                    reading_order = int(line.get_reading_order()) - empty_lines
                    reading_order_str = str(
                        reading_order)

                    line_opts = line.get_options()

                    if os.path.basename(imgfile) in self.big_initial_data and str(reading_order) == self.big_initial_data[os.path.basename(imgfile)]['place_at']:
                        big_initial = Helpers.replace_bold_starters(self.big_initial_data[os.path.basename(imgfile)]['letter'], False, cmyk=self.is_cmyk)
                        big_initial_indent = stringWidth(self.big_initial_data[os.path.basename(imgfile)]['letter'], "Junicode", self.font_size_big_initial) + 10

                    if os.path.basename(imgfile) in self.big_initial_data and j == 0 and int(self.big_initial_data[os.path.basename(imgfile)]['place_at']) < 0:
                        add_lines = int(self.big_initial_data[os.path.basename(imgfile)]['place_at']) * -1

                        for x in range(add_lines):
                            junicode_line_table_data.append(
                                ["", Spacer(0, 0), Paragraph(self.big_initial_data[os.path.basename(imgfile)]['letter'], style=style_sheet['JunicodeBigInitial']) if x == add_lines - 1 else ""])

                    if "endOfStrophe" in line_opts:
                        pivot_append_value = (j+1) if (int(line_opts['endOfStrophe'][0]["offset"]) + int(line_opts['endOfStrophe'][0]["length"])) >= len(line.get_junicode_formatted()) else j
                        if pivot_append_value not in pivot_left_alignment_positions:
                            try:
                                upcoming_pivot_line = region.get_lines()[j+1]

                                if ("fol. CCXXXIIIIrb ll. 35–64" not in imgfile) and (int(line_opts['endOfStrophe'][0]["offset"]) + int(line_opts['endOfStrophe'][0]["length"])) >= len(line.get_junicode_formatted()) and \
                                        (upcoming_pivot_line.get_junicode_formatted() == "" or "incipit" in upcoming_pivot_line.get_options() or "explicit" in upcoming_pivot_line.get_options()):
                                    is_append_pivot = False
                                else:
                                    if "fol. CCXXXIIIIrb ll. 35–64" in imgfile and pivot_append_value == 15:
                                        pivot_append_value = 19
                                    is_append_pivot = True
                            except:
                                if (int(line_opts['endOfStrophe'][0]["offset"]) + int(
                                        line_opts['endOfStrophe'][0]["length"])) >= len(
                                        line.get_junicode_formatted()):
                                    is_append_pivot = False
                                else:
                                    is_append_pivot = True

                            if is_append_pivot:
                                pivot_left_alignment_positions.append(pivot_append_value)
                                pivot_left_strophe_numbers.append(bytes(line_opts['endOfStrophe'][0]["value"], 'utf-8').decode('unicode-escape'))

                        if j == len(region.get_lines()) - 1 and (int(line_opts['endOfStrophe'][0]["offset"]) + int(line_opts['endOfStrophe'][0]["length"])) >= len(line.get_junicode_formatted()):
                            start_with_zero_pivot = True

                    elif "bigInitial" in line_opts:
                        if j not in pivot_left_alignment_positions:
                            pivot_left_alignment_positions.append(j)
                    elif ("incipit" in line_opts or "explicit" in line_opts) and last_pivot_was_incipit is False:
                        if j not in pivot_left_alignment_positions:
                            pivot_left_alignment_positions.append(j)
                        last_pivot_was_incipit = True

                    if "incipit" not in line_opts and "explicit" not in line_opts:
                        last_pivot_was_incipit = False

                    if "bigInitial" in line_opts and "height" in line_opts['bigInitial'][0]:
                        big_initial_offset = 0
                        if "offsetText" in line_opts['bigInitial'][0]:
                            big_initial_offset = int(line_opts['bigInitial'][0]['offsetText'])
                        indent_lines = max(int(line_opts['bigInitial'][0]['height']) - big_initial_offset, 3)
                        junicode = line.get_junicode_formatted()
                        big_initial = Helpers.replace_bold_starters(junicode[0], False, cmyk=self.is_cmyk)
                        junicode = junicode[1::]
                        big_initial_indent = stringWidth(big_initial if os.path.basename(imgfile) not in self.big_initial_data else self.big_initial_data[os.path.basename(imgfile)]['letter'], "Junicode", self.font_size_big_initial) + 10

                    elif "incipit" in line_opts or "explicit" in line_opts:
                        if self.is_cmyk:
                            junicode = "<font color='CMYKColor(0,1,0.5,0)' face='Junicode'>" + line.get_junicode_formatted() + "</font>"
                        else:
                            junicode = "<font color='red' face='Junicode'>" + line.get_junicode_formatted() + "</font>"
                    else:
                        junicode = line.get_junicode_formatted()

                    if line.get_junicode_formatted() != "":
                        counting_leading_empty_lines = False
                        to_append = Paragraph(
                            Helpers.replace_bold_starters(Helpers.replace_superscripts_capital_letters(
                                Helpers.replace_helper_chars(junicode)), cmyk=self.is_cmyk),
                            style=style_sheet['Junicode'])

                        str_len_to_append = stringWidth(Helpers.replace_superscripts_capital_letters(Helpers.replace_helper_chars(line.get_junicode_formatted().replace('<strike>', '').replace('</strike>', ''))), "Junicode", self.font_size)
                        str_len_reading_order = stringWidth(reading_order_str, "Junicode", self.font_size)

                        if (indent_lines > 0 and os.path.basename(imgfile) not in self.big_initial_data) \
                                or (os.path.basename(imgfile) in self.big_initial_data and str(reading_order) in self.big_initial_data[os.path.basename(imgfile)]['skip_lines']):

                            if "fol. XCVva ll. 1–34" in imgfile and (line.get_reading_order() == "29"):
                                big_initial_indent += 3.5

                            to_append = Table(
                                [(Paragraph(big_initial if os.path.basename(imgfile) not in self.big_initial_data or str(reading_order) == self.big_initial_data[os.path.basename(imgfile)]['place_at'] else '', style=style_sheet['JunicodeBigInitial']), to_append)],
                                rowHeights=average_spacing, colWidths=[big_initial_indent, (
                                        table_text_column_space_first - big_initial_indent)])

                            to_append.setStyle(TableStyle([
                                ('VALIGN', (0, 0), (-1, -1), 'BOTTOM'),
                                ('LEFTPADDING', (0, 0), (-1, -1), 0),
                                ('TOPPADDING', (0, 0), (-1, -1), 0),
                                ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                                ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                            ]))

                            str_len_to_append = str_len_to_append + big_initial_indent

                        if str_len_to_append >= self.__max_string_width['left']:
                            self.__max_string_width['fol_left'] = self.__current_file
                            self.__max_string_width['line_left'] = line.get_junicode()
                            self.__max_string_width['left'] = str_len_to_append

                        if str_len_reading_order >= self.__max_string_width['left_number']:
                            self.__max_string_width['left_number'] = str_len_reading_order

                        junicode_line_table_data.append(
                            [reading_order_str, Spacer(0, 0), to_append])
                        indent_lines = indent_lines - 1
                        big_initial = ""
                    else:
                        junicode_line_table_data.append(["", Spacer(0, 0), ""])
                        if counting_leading_empty_lines is True:
                            leading_empty_lines += 1

                verses = self.__parse_verse_lines(region.get_lines())

                self.__current_lines = []
                empty_lines_index = 0
                for j, line in enumerate(region_lines):
                    relative_offset_image = (j + empty_lines_index) * average_spacing + pdf_image_height / img_height * \
                                            (region_lines[0].get_baseline()[
                                                 'min_y'] - 30)
                    self.__current_lines.append({"line": line, "relative_offset_image": relative_offset_image})

                template = PageTemplate(id='imagepage', frames=[frame_image], onPage=self.__draw_static)
                text_template = PageTemplate(id='textpage', frames=[frame], onPage=self.__draw_text_page)
                normal_template = PageTemplate(id='normalpage', frames=[frame], onPage=self.__header_footer)

                document.addPageTemplates([template, normal_template, text_template])

                page_flow = [NextPageTemplate('imagepage'), Spacer(0, self.image_spacer_y), pdf_image,
                             Spacer(0, (self.image_caption_space - 1) / 25.4 * 72),
                             Paragraph(os.path.splitext(os.path.basename(imgfile))[0], style=style_sheet["Center"]),
                             NextPageTemplate('textpage'), PageBreak()]

                junicode_table_data = []
                repeat_verse_number = False

                if self.has_strophes:
                    for x in range(leading_empty_lines):
                        junicode_table_data.append(['', Spacer(0, 0), ''])

                prefix_was_used = False

                for i, verse in enumerate(verses):
                    if verse['is_last']:
                        if suffix_verse is not None:
                            if (verse['is_incipit'] is False and suffix_verse['is_incipit'] is False) or (
                                    verse['is_incipit'] is True and suffix_verse['is_incipit'] is True):
                                verses[i]['verse'] = Helpers.replace_punctures(
                                    verse['verse'] + " " + suffix_verse['verse'])
                        if suffix_verse_num != 0:
                            verses[i]['verse_number'] = suffix_verse_num
                        if suffix_strophe_num != 0:
                            verses[i]['strophe_number'] = suffix_strophe_num

                    if i == 0 and verse['is_first'] and prefix_verse is not None and prefix_verse['verse'] != "":
                        if (verse['is_incipit'] is False and prefix_verse['is_incipit'] is False) or (
                                verse['is_incipit'] is True and prefix_verse['is_incipit'] is True):
                            verses[i]['verse'] = Helpers.replace_punctures(prefix_verse['verse'] + " " + verse['verse'])
                            prefix_was_used = True

                for i, verse in enumerate(verses):
                    junicode_table_data_iteration = []
                    if verse['is_incipit']:
                        if i != 0 and verses[i - 1]['is_incipit'] is False:
                            junicode_table_data_iteration.append(['', ''])

                        incipit_number = 'i'
                        if verse['aventiure'] != -1:
                            incipit_number = incipit_number + verse['aventiure']
                        junicode_table_data_iteration.append([incipit_number, Helpers.replace_helper_chars(verse['verse'])])
                        if i < len(verses) - 1 and verses[i + 1]['is_incipit'] is False:
                            junicode_table_data_iteration.append(['', ''])
                    else:
                        if verse['text_start']:
                            self.verse_counter = 0
                            self.strophe_counter = 1
                            self.set_strophe_number = True

                        if self.has_strophes is False:
                            try:
                                next_verse_num = verses[i + 1]['verse_number']
                                is_next_verse_incipit = verses[i + 1]['is_incipit']
                                if i > 0:
                                    is_prev_verse_incipit = verses[i - 1]['is_incipit']
                                else:
                                    is_prev_verse_incipit = False
                            except IndexError:
                                next_verse_num = "0"
                                is_prev_verse_incipit = False
                                is_next_verse_incipit = False

                            if Helpers.represents_int(verse['verse_number']):
                                if Helpers.represents_int(next_verse_num):
                                    next_verse_integer = int(next_verse_num)
                                else:
                                    next_verse_integer = 0

                                verse_integer = int(verse['verse_number'])

                                if i == 0:
                                    self.verse_counter = verse_integer - 1
                                verse_num_text = '{:,}'.format(verse_integer).replace(',', ' ') if (
                                        verse_integer % 5 == 0 or self.verse_counter == 0 or (Helpers.represents_int(next_verse_num) is False and is_next_verse_incipit is False) or (
                                        verse_integer != next_verse_integer - 1 and next_verse_integer != 0
                                                and is_prev_verse_incipit is False
                                                and is_next_verse_incipit is False)
                                                or (repeat_verse_number is True)) else ""
                                if verse_integer != next_verse_integer - 1 and next_verse_integer != 0 \
                                        and is_prev_verse_incipit is False and is_next_verse_incipit is False:
                                    repeat_verse_number = True
                                    self.verse_counter = verse_integer - 1
                                else:
                                    repeat_verse_number = False
                            else:
                                verse_num_text = Helpers.set_thousand_spaces(verse['verse_number'])
                                repeat_verse_number = True

                            str_len_verse_num_text = stringWidth(verse_num_text, "Junicode", self.font_size)

                            if str_len_verse_num_text >= self.__max_string_width['right_number']:
                                self.__max_string_width['right_number'] = str_len_verse_num_text

                            junicode_table_data_iteration.append(
                                [verse_num_text, Helpers.replace_helper_chars(verse['verse'])])

                            self.verse_counter += 1
                        else:
                            if self.set_strophe_number:
                                if i == 0 and prefix_was_used is False and previous_ended_strophe is False and start_of_text is False:
                                    strophe_num = ""
                                else:
                                    for j, strophe in enumerate(verses):
                                        if j < i:
                                            continue
                                        if strophe['strophe_end']:
                                            if Helpers.represents_int(strophe['strophe_number']):
                                                self.strophe_integer = int(strophe['strophe_number'])
                                            else:
                                                self.strophe_integer = bytes(strophe['strophe_number'], 'utf-8').decode('unicode-escape')
                                            break
                                        if j == len(verses)-1 and strophe['strophe_end'] is False:
                                            self.strophe_integer = next_textregion_first_strophe

                                    if isinstance(self.strophe_integer, int):
                                        strophe_num = '{:,}'.format(self.strophe_integer).replace(',', ' ')
                                    else:
                                        strophe_num = Helpers.set_thousand_spaces(self.strophe_integer)
                                    if i != 0 and verses[i - 1]['is_incipit'] is False:
                                        junicode_table_data_iteration.append(['', ''])
                                start_of_text = False
                            else:
                                strophe_num = ""

                            if verse['strophe_end']:
                                self.set_strophe_number = True

                                self.strophe_counter += 1
                            else:
                                if i != len(verses) - 1 or (suffix_verse is not None and suffix_verse['verse'] == ""):
                                    self.set_strophe_number = False

                            str_len_verse_num_text = stringWidth(strophe_num, "Junicode", self.font_size)

                            if str_len_verse_num_text >= self.__max_string_width['right_number']:
                                self.__max_string_width['right_number'] = str_len_verse_num_text

                            if verse['aventiure'] != -1:
                                strophe_num = strophe_num + "%a" + verse['aventiure']

                            junicode_table_data_iteration.append(
                                [strophe_num, Helpers.replace_helper_chars(verse['verse'])])

                    for append in junicode_table_data_iteration:
                        if "i" in append[0] and append[0][0] == "i":
                            incipit_number = append[0].replace('i', '')

                            if incipit_number != '':
                                if os.path.basename(imgfile) in self.skip_aventiures_data:
                                    junicode_table_data = self.__set_aventiure_line(junicode_table_data, incipit_number, style_sheet['Junicode'])

                                    incipit_number = ''
                                else:
                                    incipit_number = Paragraph(("<font color='red' face='Junicode' size='" + str(self.font_size_additives) + "'>" if self.is_cmyk is False else "<font color='CMYKColor(0,1,0.5,0)' face='Junicode' size='" + str(self.font_size_additives) + "'>") + incipit_number + '</font>', style=style_sheet['JunicodeRightSmall'])

                            if skip_right_pivot is False:
                                pivot_right_alignment_positions.append(len(junicode_table_data))
                            else:
                                skip_right_pivot = False
                            incipit_excipit_split_lines = self.__split_line(line=append[1],
                                                                            container_width=table_text_column_space_second - self.superscript_buffer, is_incipit=True,)

                            for inc_exc_ind, inc_exc_line in enumerate(incipit_excipit_split_lines):
                                if inc_exc_line.strip() == "":
                                    continue
                                junicode_table_data.append(
                                    [incipit_number if inc_exc_ind == 0 else '', Spacer(0, 0), Paragraph(
                                        ("<font color='red' face='Junicode'>" if self.is_cmyk is False else "<font color='CMYKColor(0,1,0.5,0)' face='Junicode'>") + Helpers.replace_superscripts_capital_letters(Helpers.replace_superscripts_quotable(
                                            Helpers.replace_spaces_with_entities_chars(
                                                inc_exc_line.strip()))) + "</font>",
                                        style=style_sheet['Junicode'])])
                        else:
                            if "%a" in append[0]:
                                position = append[0].find("%a")
                                aventiure_val = append[0][position+2:]

                                if os.path.basename(imgfile) in self.skip_aventiures_data:
                                    junicode_table_data = self.__set_aventiure_line(junicode_table_data, aventiure_val, style_sheet['Junicode'])

                                if position > 0:
                                    append[0] = append[0][0:position]
                                else:
                                    append[0] = ""

                            split_lines = self.__split_line(line=append[1],
                                                            container_width=table_text_column_space_second - self.superscript_buffer, print_widths=("fol. CIIIvc ll. 35–43" in imgfile))

                            if self.has_strophes:
                                if append[0] != "":
                                    if skip_right_pivot is False and (i != 0 or prefix_was_used is False or previous_ended_strophe):
                                        pivot_right_alignment_positions.append(len(junicode_table_data))
                                    else:
                                        skip_right_pivot = False
                                else:
                                    for char in Helpers.get_capital_letter_replacement_array():
                                        if char in append[1]:
                                            if skip_right_pivot is False:
                                                pivot_right_alignment_positions.append(len(junicode_table_data))
                                            else:
                                                skip_right_pivot = False

                            if "im waren ze dreyen jaren" in split_lines[0]:
                                continue

                            if "im waren ze dreyen Jaren" in split_lines[0]:
                                split_lines[0] = "im waren ze dreyen Jaren    die kiele wol geladen"

                            par = "<nobr>" + Helpers.replace_bold_starters(
                                                                                 Helpers.replace_superscripts_capital_letters(Helpers.replace_superscripts_quotable(
                                                                                     Helpers.replace_spaces_with_entities_chars(
                                                                                         split_lines[0]))), cmyk=self.is_cmyk) + "</nobr>"

                            str_len_verse_num_text = stringWidth(append[0], "Junicode", self.font_size)

                            if str_len_verse_num_text >= self.__max_string_width['right_number']:
                                self.__max_string_width['right_number'] = str_len_verse_num_text

                            junicode_table_data.append([append[0], Spacer(0, 0), Paragraph(par,
                                                                             style=style_sheet['Junicode'])])

                            for ind, line in enumerate(split_lines):
                                if ind == 0:
                                    continue
                                if "    " not in line:
                                    line = line.strip()

                                par = Helpers.replace_bold_starters(
                                        Helpers.replace_superscripts_capital_letters(Helpers.replace_superscripts_quotable(
                                            Helpers.replace_spaces_with_entities_chars(line))), cmyk=self.is_cmyk)

                                junicode_table_data.append(
                                    ['', Spacer(0, 0), Paragraph(par,
                                        style=style_sheet['JunicodeRight'])])

                if region.get_has_amen():
                    junicode_table_data.append(['', Spacer(0, 0), ''])
                    junicode_table_data.append(
                        ['', Spacer(0, 0), Paragraph("&nbsp;&nbsp;&nbsp;&nbsp;Amen",
                            style=style_sheet['Junicode'])])

                lines_without_empty = region.get_lines_without_empty()
                for j, line in enumerate(lines_without_empty):
                    line_opts = line.get_options()
                    if "endOfStrophe" in line_opts:
                        if j == len(lines_without_empty) - 1 and (int(line_opts['endOfStrophe'][0]["offset"]) + int(line_opts['endOfStrophe'][0]["length"])) >= len(line.get_junicode_formatted()):
                            previous_ended_strophe = True
                        else:
                            previous_ended_strophe = False
                    elif j == len(lines_without_empty) - 1:
                        if "incipit" in line_opts:
                            previous_ended_strophe = True
                        else:
                            previous_ended_strophe = False

                table_styles = [('ALIGN', (0, 0), (0, -1), 'RIGHT'),
                                ('ALIGN', (1, 0), (1, -1), 'LEFT'),
                                ('SIZE', (1, 0), (1, -1), self.font_size),
                                ('FONT', (1, 0), (1, -1), 'Junicode'),
                                ('FONT', (0, 0), (0, -1), 'Junicode'),
                                ('SIZE', (0, 0), (0, -1), self.font_size_verse_numbers),
                                ('VALIGN', (0, 0), (-1, -1), 'BOTTOM'),
                                ('LEFTPADDING', (0, 0), (-1, -1), 0),
                                ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                                ]

                table_space = self.header_line_verses_space / 25.4 * 72 - 5.6

                page_flow.append(Spacer(0, table_space))

                first_line = self.__current_lines[0]

                standard_image_offset = 13

                line_spacing = first_line[
                                   'relative_offset_image'] - table_space + self.font_size + standard_image_offset

                negative_spacer_offset = (line_spacing - 39.98425196850393) + (
                        -4.57 - average_spacing * 4 + 14.814545454545454 * 4) if self.is_right_side_line_height_static is False else 0

                if os.path.basename(imgfile) in self.big_initial_data and int(
                        self.big_initial_data[os.path.basename(imgfile)]['place_at']) < 0:
                    add_lines = int(self.big_initial_data[os.path.basename(imgfile)]['place_at']) * -1
                    big_initial_offset_spacer = add_lines * (
                        self.table_row_height if self.is_right_side_line_height_static else average_spacing)
                else:
                    big_initial_offset_spacer = 0

                right_spacer_height = max(0, (line_spacing - ((self.table_row_height - self.font_size) + (
                        average_spacing - self.font_size) if self.is_right_side_line_height_static else 0))) if self.has_strophes else negative_spacer_offset

                # Insert lines before calculation of right_side_static
                if self.has_strophes is False or self.column_title == "RABENSCHLACHT":
                    junicode_table_data = self.__insert_csv_offsets(junicode_table_data, imgfile)

                if self.has_strophes is False:
                    for add_empty_line_index in range(leading_empty_lines):
                        junicode_table_data.insert(0, ['', Spacer(0, 0), ''])

                    height_right_side = average_spacing * len(junicode_table_data)

                    removed_lines_offset = 0

                    right_side_static = self.max_height_lines < (height_right_side - removed_lines_offset)
                else:
                    if self.column_title == "RABENSCHLACHT":
                        removed_lines_offset = 3*average_spacing

                        height_right_side = average_spacing * len(junicode_table_data)

                        right_side_static = self.max_height_lines < (height_right_side - removed_lines_offset)
                    else:
                        if len(pivot_left_alignment_positions) < len(pivot_right_alignment_positions):
                            pivot_right_alignment_positions = pivot_right_alignment_positions[1:]

                        first_pivot_right_at = pivot_right_alignment_positions[0]

                        first_pivot_left_at = pivot_left_alignment_positions[0]

                        pivot_offset = (first_pivot_right_at-first_pivot_left_at)

                        added_pivot_lines = 0

                        if pivot_offset < 0:
                            pivot_offset = abs(pivot_offset)
                            if first_pivot_right_at > 0:
                                for add_line_index in range(pivot_offset):
                                    junicode_table_data.insert(first_pivot_right_at, ['', Spacer(0, 0), ''])
                                    added_pivot_lines += 1
                            removed_lines_offset = 0
                            pivot_offset = pivot_offset * -1
                        else:
                            removed_lines_offset = pivot_offset*average_spacing

                        pivot_right_alignment_positions_without_first = pivot_right_alignment_positions[1:]
                        pivot_left_alignment_positions_without_first = pivot_left_alignment_positions[1:]

                        pivot_offset = pivot_offset*-1

                        if len(pivot_right_alignment_positions_without_first) > 0:
                            for pivot_ind, position in enumerate(pivot_right_alignment_positions[1:]):
                                add_lines_before_position = max(0, (pivot_left_alignment_positions_without_first[pivot_ind] - (position+pivot_offset)))
                                pivot_offset+=add_lines_before_position
                                for add_line_index in range(add_lines_before_position):
                                    junicode_table_data.insert(position + added_pivot_lines, ['', Spacer(0, 0), ''])
                                    added_pivot_lines+=1

                        right_side_static = False

                right_spacer_height = right_spacer_height - removed_lines_offset

                # if text has strophes we have to do alignment before inserting lines
                # right side caculation is not important since its always static
                if self.has_strophes is True and self.column_title != "RABENSCHLACHT":
                    junicode_table_data = self.__insert_csv_offsets(junicode_table_data, imgfile)

                right_spacer_height = right_spacer_height + region.get_right_offset() * average_spacing

                if right_side_static:
                    row_height = float(self.row_height_data[str(len(junicode_table_data))]['height'])
                    right_spacer_height = -8/15.08 * row_height
                else:
                    row_height = average_spacing

                    if "fol. XCVrb ll. 1–34" in imgfile:
                        temp = junicode_table_data[10]
                        junicode_table_data[10] = junicode_table_data[9]
                        junicode_table_data[9] = temp

                    if "fol. CIIIvc ll. 35–43" in imgfile:
                        temp = junicode_table_data[32]
                        junicode_table_data[32] = junicode_table_data[7]
                        junicode_table_data[7] = temp

                table_lines = Table(junicode_line_table_data, rowHeights=average_spacing,
                                    colWidths=[table_column_space_numerations_1, self.spacing_between_table_columns, table_text_column_space_first, ])
                table_verses = Table(junicode_table_data,
                                     rowHeights=row_height,
                                     colWidths=[table_column_space_numerations_2, self.spacing_between_table_columns, table_text_column_space_second])

                if len(junicode_line_table_data) > self.__row_count['left']:
                    self.__row_count['left'] = len(junicode_line_table_data)

                if len(junicode_table_data) > self.__row_count['right']:
                    self.__row_count['right'] = len(junicode_table_data)

                table_lines.setStyle(TableStyle(table_styles))
                table_verses.setStyle(TableStyle(table_styles))

                table = Table([([Spacer(0, max(0, line_spacing - big_initial_offset_spacer)), table_lines],
                                [Spacer(0, right_spacer_height),
                                 table_verses])],
                              colWidths=[(table_column_space_numerations_1 + self.spacing_between_table_columns + table_text_column_space_first),
                                         (table_column_space_numerations_2 + self.spacing_between_table_columns + table_text_column_space_second)])

                table.setStyle(TableStyle([
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                    ('LEFTPADDING', (0, 0), (-1, -1), 0),
                    ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                ]))

                page_flow.append(table)

                try:
                    document.build(page_flow)
                except:
                    self.__max_string_width['error_pages'].append(xmlfile)

                self.page_number += 2

            except IndexError:
                continue

        if not os.path.exists('output_pdf/length'):
            os.makedirs('output_pdf/length')

        f = open('output_pdf/length/' + self.column_title + ".txt", "a")
        out = self.column_title + "\nLeft Number Max Width: " + str(self.__max_string_width['left_number']) + \
            "\nLeft Text Max Width: " + str(self.__max_string_width['left']) + \
            "\nLeft File Max Width: " + str(self.__max_string_width['fol_left']) + \
            "\nLeft Line Max Width: " + str(self.__max_string_width['line_left']) + \
            "\n\nRight Number Max Width: " + str(self.__max_string_width['right_number']) + \
            "\nRight Text Max Width: " + str(self.__max_string_width['right']) + \
            "\nRight File Max Width: " + str(self.__max_string_width['fol_right']) + \
            "\nRight Line Max Width: " + str(self.__max_string_width['line_right']) + \
            "\nRight Line Index: " + str(self.__max_string_width['line_right_index']) + \
            "\nError Pages: " + str(self.__max_string_width['error_pages']) + \
            "\n\nLeft Lines: " + str(self.__row_count['left']) + \
            "\nRight Lines: " + str(self.__row_count['right'])
        f.write(out)
        f.close()

        return self.page_number

    def __set_aventiure_line(self, junicode_table_data, incipit_number, style_sheet):
        add_aventiure_line = ['', Spacer(0, 0), Paragraph(
            ("<font color='red' face='Junicode'>" if self.is_cmyk is False else "<font color='CMYKColor(0,1,0.5,0)' face='Junicode'>[") + incipit_number + ". <font face='JunicodeIt'>âventiure</font>]</font>",
            style=style_sheet) ]
        if len(junicode_table_data) < 3:
            junicode_table_data.append(add_aventiure_line)
            junicode_table_data.append(['', Spacer(0, 0), ''])
        else:
            try:
                to_check_first = junicode_table_data[len(junicode_table_data) - 1][2].text.replace('<nobr>', '').replace('</nobr>', '')
                to_check_second = junicode_table_data[len(junicode_table_data) - 2][2].text.replace('<nobr>', '').replace('</nobr>', '')
                to_check_third = junicode_table_data[len(junicode_table_data) - 3][2].text.replace('<nobr>', '').replace('</nobr>', '')
            except:
                to_check_first = junicode_table_data[len(junicode_table_data) - 1][2].replace('<nobr>', '').replace('</nobr>', '')
                to_check_second = junicode_table_data[len(junicode_table_data) - 2][2].replace('<nobr>', '').replace('</nobr>', '')
                to_check_third = junicode_table_data[len(junicode_table_data) - 3][2].replace('<nobr>', '').replace('</nobr>', '')

            if to_check_first != '':
                junicode_table_data.append(['', Spacer(0, 0), ''])
                junicode_table_data.append(add_aventiure_line)
                junicode_table_data.append(['', Spacer(0, 0), ''])
            elif to_check_second != '':
                junicode_table_data.append(add_aventiure_line)
                junicode_table_data.append(['', Spacer(0, 0), ''])
            elif to_check_third != '':
                junicode_table_data.append(['', Spacer(0, 0), ''])
                junicode_table_data[len(junicode_table_data) - 2] = add_aventiure_line
            else:
                junicode_table_data[len(junicode_table_data) - 2] = add_aventiure_line

        return junicode_table_data

    def __insert_csv_offsets(self, junicode_table_data, imgfile):
        for offset_information in self.offset_data:
            if offset_information['file'] in imgfile:
                for csv_offset in offset_information['lines']:
                    for x in range(csv_offset['line']):
                        junicode_table_data.insert(csv_offset['line-insert'], ['', Spacer(0, 0), ''])
                continue

        return junicode_table_data

    def __parse_verse_lines(self, lines):
        verse_lines = []
        current_line = ""
        strophe_end = False
        text_start = False
        is_incipit = False
        is_last = False
        is_first = True
        ret_aventiure = -1
        verse_number = ""
        strophe_number = ""
        temp_lines = []

        incipit_line = ""
        is_incipit_first = None
        is_incipit_last = None

        for index, line in enumerate(lines):
            amount_verses = line.get_verse_amount()
            temp_lines.append(line)

            for i in range(-1, amount_verses):
                verse_string, verse_end, strophe_end, text_start, is_incipit, verse_number, strophe_number, aventiure = line.get_verse_string(
                    i)
                is_last = (index + 1 == len(lines) and i == (amount_verses - 1))

                if aventiure != -1:
                    ret_aventiure = aventiure

                if is_incipit is True:
                    if is_incipit_first is None:
                        is_incipit_first = is_first
                    if is_incipit_last is None:
                        is_incipit_last = is_last
                    incipit_line += verse_string + " "
                else:
                    if incipit_line.strip() != "":
                        verse_lines.append({"verse": Helpers.post_process_verse(incipit_line.strip().replace("  ", " "), temp_lines,
                                                                                self.has_strophes is False),
                                            "strophe_end": strophe_end,
                                            "text_start": text_start, "is_incipit": True, "aventiure": ret_aventiure,
                                            "is_last": is_incipit_last, "is_first": is_incipit_first,
                                            "verse_number": verse_number,
                                            "strophe_number": strophe_number, })
                        incipit_line = ""
                        is_incipit_first = None
                        is_incipit_last = None
                        ret_aventiure = -1

                    current_line += verse_string

                    if verse_end is True:
                        verse_lines.append({"verse": Helpers.post_process_verse(current_line.strip(), temp_lines,
                                                                                self.has_strophes is False),
                                            "strophe_end": strophe_end,
                                            "text_start": text_start, "is_incipit": is_incipit, "aventiure": ret_aventiure,
                                            "is_last": is_last, "is_first": is_first, "verse_number": verse_number,
                                            "strophe_number": strophe_number, })
                        temp_lines = [line]
                        is_first = False
                        current_line = ""
                        ret_aventiure = -1

        if incipit_line.strip() != "":
            verse_lines.append(
                {"verse": Helpers.post_process_verse(incipit_line.strip().replace("  ", " "), temp_lines, self.has_strophes is False),
                 "strophe_end": strophe_end,
                 "text_start": text_start, "is_incipit": True, "aventiure": ret_aventiure,
                 "is_last": is_incipit_last, "is_first": is_incipit_first, "verse_number": verse_number,
                 "strophe_number": strophe_number, })

        if current_line.strip() != "":
            verse_lines.append(
                {"verse": Helpers.post_process_verse(current_line.strip(), temp_lines, self.has_strophes is False),
                 "strophe_end": strophe_end,
                 "text_start": text_start, "aventiure": ret_aventiure,
                 "is_incipit": is_incipit, "is_last": is_last, "is_first": is_first, "verse_number": verse_number,
                 "strophe_number": strophe_number, })

        return verse_lines

    def __split_line(self, line, container_width, is_incipit = False, print_widths=False):
        string_width = stringWidth(line, "Junicode", self.font_size)

        if string_width <= container_width:
            if string_width >= self.__max_string_width['right'] and line.isspace() is False and is_incipit is False:
                self.__max_string_width['fol_right'] = self.__current_file
                self.__max_string_width['line_right'] = line
                self.__max_string_width['line_right_index'] = self.verse_counter
                self.__max_string_width['right'] = string_width
            return [line]

        try:
            new_line = ""
            while string_width > container_width:
                split = line.rsplit(' ', 1)
                new_line = split[1] + ' ' + new_line
                line = split[0]
                string_width = stringWidth(line, "Junicode", self.font_size)
        except IndexError:
            return [line]

        new_line = new_line + "    " + "    " + "    " + "    "

        return reduce(list.__add__, [[line], self.__split_line(new_line, container_width)])

    def __header_footer(self, canvas, doc):
        canvas.saveState()
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Right', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_RIGHT))
        styles.add(ParagraphStyle(name='Left', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_LEFT))

        # Header
        text_y = self.canvas_size[1] - self.header_text_page_top_space / 25.4 * 72 - self.font_size_additives
        line_y = text_y - Helpers.mm_to_pts(self.header_text_line_space)

        header = Paragraph(self.column_title, styles['Left'])
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, doc.leftMargin, text_y)
        header2 = Paragraph(str(self.page_number + 1), styles['Right'])
        w, h = header2.wrap(doc.width, doc.topMargin)
        header2.drawOn(canvas, doc.leftMargin - self.header_line_outside_offset, text_y)
        canvas.scale(self.line_scale, self.line_scale)
        canvas.line(1 / self.line_scale * doc.leftMargin, 1 / self.line_scale * line_y,
                    1 / self.line_scale * (
                                doc.width + doc.leftMargin) - 1 / self.line_scale * self.header_line_outside_offset,
                    1 / self.line_scale * line_y)
        canvas.scale(1, 1)

        # Footer
        footer = Paragraph('', styles['Normal'])
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, doc.leftMargin, h)

        canvas.restoreState()

    def __is_part_of_cluster(self, cluster, line):
        margin = 40

        return (line['relative_offset_image']) >= (cluster['min_y'] - margin) \
               and (line['relative_offset_image'] + self.table_row_height) <= (cluster['max_y'] + margin)

    def __get_cluster_line_height(self, cluster):
        return max(self.table_row_height, (cluster['max_y'] - cluster['min_y']) / cluster['amount_of_non_folio_lines'])

    def __get_clusters(self, lines):
        clusters = []

        try:
            for index, line in enumerate(lines):
                cluster_found = False
                cluster_index = -1
                min_y = 9999999
                max_y = -1

                for j, cluster in enumerate(clusters):
                    if self.__is_part_of_cluster(cluster, line):
                        cluster_found = True
                        cluster_index = j
                        min_y = cluster['min_y'] if cluster['min_y'] < (line['relative_offset_image']) else line['relative_offset_image']
                        max_y = cluster['max_y'] if cluster['max_y'] > (line['relative_offset_image'] + self.table_row_height) else (line['relative_offset_image'] + self.table_row_height)
                        break

                if cluster_found:
                    clusters[cluster_index]['lines'].append(line)
                    clusters[cluster_index]['amount_of_non_folio_lines'] += 0 if line["line"].get_is_folio() else 1
                    clusters[cluster_index]['min_y'] = min_y
                    clusters[cluster_index]['max_y'] = max_y
                else:
                    clusters.append({'lines': [line], 'max_y': (line['relative_offset_image'] + self.table_row_height), 'min_y':(line['relative_offset_image']), 'amount_of_non_folio_lines': 0 if line["line"].get_is_folio() else 1})
            return clusters
        except:
            return clusters

    def __draw_text_page_absolute(self, canvas, doc):
        canvas.saveState()

        clusters = self.__get_clusters(self.__current_lines)

        folios = 0
        try:
            for cluster in clusters:
                right_full_junicode = ""
                biggest_line = 0
                added_lines = 0
                line_height = self.__get_cluster_line_height(cluster) if cluster['amount_of_non_folio_lines'] > 1 else (self.table_row_height + 5)
                string_width = 10
                placement_x = Helpers.mm_to_pts(self.margin_left) + 11.4 + string_width - 3.27 # 11.4
                placement_x_normalized = placement_x + 260 + 20 + 3.27 - 4.5  # 250 is biggest line
                placement_y_first_cluster_line = -1
                for i, line in enumerate(cluster['lines']):
                    if line["line"].get_junicode() == "":
                        continue

                    if line["line"].get_is_folio():
                        folios += 1

                    reading_order = (int(line["line"].get_reading_order()) - folios) if line["line"].get_is_folio() is False else ''

                    left_align = 10 - canvas.stringWidth(str(reading_order), "Junicode",
                                                         self.font_size)
                    string_width_junicode = canvas.stringWidth(line['line'].get_junicode(), "Junicode",
                                                               self.font_size)
                    biggest_line = biggest_line if string_width_junicode <= biggest_line else string_width_junicode

                    placement_y = self.canvas_size[1] - Helpers.mm_to_pts(self.margin_image_vertical) - (cluster[
                                                                                                             "min_y"] + line_height * i) - self.image_spacer_y - self.font_size
                    if placement_y_first_cluster_line == -1:
                        placement_y_first_cluster_line = placement_y
                    placement_x += 0 if line["line"].get_is_folio() is False else (250 - string_width_junicode)
                    placement_x_reading_order = Helpers.mm_to_pts(self.margin_left) + left_align - 3.32

                    placement_x_reading_order_normalized = placement_x_normalized - string_width - 10 + left_align

                    canvas.setFont("Junicode", self.font_size_additives)
                    canvas.setFillColorCMYK(0, 0, 0, 1)

                    canvas.drawString(placement_x_reading_order, placement_y, str(reading_order))

                    if "incipit" in line["line"].get_options():
                        canvas.setFillColorCMYK(0, 1, 1, 0)
                    else:
                        canvas.setFillColorCMYK(0, 0, 0, 1)

                    canvas.setFont("Junicode", self.font_size)

                    canvas.drawString(placement_x, placement_y, Helpers.replace_superscripts_capital_letters(line['line'].get_junicode().strip()))

                    if line["line"].get_is_folio() is False:
                        processed_junicode = Helpers.post_process_verse(line['line'].get_junicode().strip(), list(map(lambda x: x['line'], self.__current_lines)), False, True)
                        right_junicode = Helpers.replace_bold_starters(
                            Helpers.replace_superscripts_capital_letters(Helpers.replace_superscripts_quotable(
                                Helpers.replace_spaces_with_entities_chars(processed_junicode))), cmyk=self.is_cmyk)
                        right_full_junicode += " " + right_junicode.strip()

                    canvas.setFont("Junicode", self.font_size)

                splitted_lines = self.__split_line(Helpers.replace_punctures(right_full_junicode), 210 + 4.5)

                for split_index, splitted_line in enumerate(splitted_lines):
                    placement_y_left = placement_y_first_cluster_line - line_height * added_lines
                    added_lines += 1
                    canvas.drawString(placement_x_normalized, placement_y_left, splitted_line.strip())
        except:
            print("Error")
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Right', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_RIGHT))
        styles.add(ParagraphStyle(name='Left', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_LEFT))

        # Header
        text_y = self.canvas_size[1] - self.header_text_page_top_space / 25.4 * 72 - self.font_size_additives
        line_y = text_y - Helpers.mm_to_pts(self.header_text_line_space)

        header = Paragraph(self.column_title, styles['Left'])  # str(canvas.getPageNumber())
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, doc.leftMargin, text_y)
        header2 = Paragraph(str(self.page_number + 1), styles['Right'])  # str(canvas.getPageNumber())
        w, h = header2.wrap(doc.width, doc.topMargin)
        header2.drawOn(canvas, doc.leftMargin - self.header_line_outside_offset, text_y)
        canvas.scale(self.line_scale, self.line_scale)
        canvas.line(1 / self.line_scale * doc.leftMargin, 1 / self.line_scale * line_y,
                    1 / self.line_scale * (
                            doc.width + doc.leftMargin) - 1 / self.line_scale * self.header_line_outside_offset,
                    1 / self.line_scale * line_y)
        canvas.scale(1, 1)

        # Footer
        footer = Paragraph('', styles['Normal'])
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, doc.leftMargin, h)

        canvas.restoreState()

    def __draw_static_absolute(self, canvas, doc):
        canvas.saveState()

        clusters = self.__get_clusters(self.__current_lines)
        folios = 0
        for cluster in clusters:
            line_height = self.__get_cluster_line_height(cluster)
            for i, line in enumerate(cluster['lines']):
                if line["line"].get_is_folio():
                    folios += 1
                    continue

                if line["line"].get_junicode() == "":
                    continue

                reading_order = int(line["line"].get_reading_order()) - folios

                string_width = canvas.stringWidth(str(reading_order), "Junicode",
                                                  self.font_size)

                placement_y = self.canvas_size[1] - Helpers.mm_to_pts(self.margin_image_vertical) - (cluster[
                                                                                                         "min_y"] + line_height * i) - self.image_spacer_y - self.font_size
                placement_x = (doc.width / 2 + Helpers.mm_to_pts(self.margin_image_left)) - (
                    -1 if self.is_drawing_left is True else 1) * (
                                      self.__image_width / 2 + (0 if self.is_drawing_left is True else string_width) + (
                                      self.image_verse_number_space * 72 / 25.4)) + 1.57

                canvas.setFont("Junicode", self.font_size_additives)

                canvas.drawString(placement_x, placement_y, str(reading_order))

                canvas.setFont("Junicode", self.font_size_additives)

        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Right', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_RIGHT))
        styles.add(ParagraphStyle(name='Left', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_LEFT))

        # Header
        text_y = self.canvas_size[1] - self.header_text_page_top_space / 25.4 * 72 - self.font_size_additives
        line_y = text_y - Helpers.mm_to_pts(self.header_text_line_space)

        header = Paragraph(str(self.page_number), styles['Left'])  # str(canvas.getPageNumber())
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, Helpers.mm_to_pts(self.margin_image_left) + self.header_line_outside_offset / 2, text_y)
        header2 = Paragraph("TEI", styles['Right'])  # str(canvas.getPageNumber())
        w, h = header2.wrap(doc.width, doc.topMargin)
        header2.drawOn(canvas, Helpers.mm_to_pts(self.margin_image_left), text_y)
        canvas.scale(self.line_scale, self.line_scale)
        canvas.line(1 / self.line_scale * Helpers.mm_to_pts(self.margin_image_left) + self.header_line_outside_offset,
                    1 / self.line_scale * line_y,
                    1 / self.line_scale * (doc.width + Helpers.mm_to_pts(self.margin_image_left)),
                    1 / self.line_scale * line_y)
        canvas.scale(1, 1)

        canvas.restoreState()

    def __draw_static(self, canvas, doc):
        canvas.saveState()

        empty_lines = 0

        for index, line in enumerate(self.__current_lines):
            empty_lines += line["line"].get_empty_lines()

            if line["line"].get_junicode() == "":
                continue

            reading_order = int(line["line"].get_reading_order()) - empty_lines
            string_width = canvas.stringWidth(str(reading_order), "Junicode",
                                              self.font_size_additives)

            placement_y = self.canvas_size[1] - Helpers.mm_to_pts(self.margin_image_vertical) - line[
                "relative_offset_image"] - self.image_spacer_y - self.font_size
            placement_x = (doc.width / 2 + Helpers.mm_to_pts(self.margin_image_left)) - (
                -1 if self.is_drawing_left is True else 1) * (
                                      self.__image_width / 2 + (0 if self.is_drawing_left is True else string_width) + (
                                      self.image_verse_number_space * 72 / 25.4))

            canvas.setFont("Junicode", self.font_size_additives)

            canvas.drawString(placement_x, placement_y, str(reading_order))

            canvas.setFont("Junicode", self.font_size_additives)

        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Right', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_RIGHT))
        styles.add(ParagraphStyle(name='Left', fontName='Junicode', fontSize=self.font_size, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_LEFT))

        # Header
        text_y = self.canvas_size[1] - self.header_text_page_top_space / 25.4 * 72 - self.font_size_additives
        line_y = text_y - Helpers.mm_to_pts(self.header_text_line_space)

        header = Paragraph(str(self.page_number), styles['Left'])  # str(canvas.getPageNumber())
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, Helpers.mm_to_pts(self.margin_image_left) + self.header_line_outside_offset / 2, text_y)
        header2 = Paragraph("TEI", styles['Right'])  # str(canvas.getPageNumber())
        w, h = header2.wrap(doc.width, doc.topMargin)
        header2.drawOn(canvas, Helpers.mm_to_pts(self.margin_image_left), text_y)
        canvas.scale(self.line_scale, self.line_scale)
        canvas.line(1 / self.line_scale * Helpers.mm_to_pts(self.margin_image_left) + self.header_line_outside_offset,
                    1 / self.line_scale * line_y,
                    1 / self.line_scale * (doc.width + Helpers.mm_to_pts(self.margin_image_left)),
                    1 / self.line_scale * line_y)
        canvas.scale(1, 1)

        canvas.restoreState()

    def __draw_init(self, canvas, doc):
        canvas.saveState()

        placement_y = self.canvas_size[1] / 2 + self.font_size_initial + Helpers.mm_to_pts(10)

        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Center', fontName='Junicode', fontSize=25, leading=20,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_CENTER))

        title = Paragraph(self.first_page_title + self.column_title_first_page_addition, styles['Center'])
        w, h = title.wrap(doc.width, doc.topMargin)
        title.drawOn(canvas, Helpers.mm_to_pts(self.margin_image_left), placement_y)

        canvas.restoreState()

    def __draw_init_tables(self, canvas, doc):
        canvas.saveState()

        canvas.restoreState()

    def __draw_text_page(self, canvas, doc):
        canvas.saveState()

        styles = getSampleStyleSheet()

        styles.add(ParagraphStyle(name='Junicode', fontName='Junicode', fontSize=self.font_size_legend, leading=12,
                                  textColor=colors.rgb2cmyk(0, 0, 0) if self.is_cmyk is True else colors.black,
                                  alignment=TA_JUSTIFY))

        abbrevs = []

        for line in self.__current_lines:
            for line_abbreviations in line['line'].get_abbreviations():
                abbrevs.append(line_abbreviations['abbreviated'] + " = " + line_abbreviations['expansion'])

            abbreviation_string = ', '.join(abbrevs)

            # Footer
            footer = Paragraph('\u2202\u003d\u0064\u002c \u0267\u003d\u0068\u002c \u0271\u003d\u006d\u002c '
                               '\u014b\u003d\u006e\u002c \ua75b\u003d\u0072\u002c \u025e\u003d\u0073\u002c '
                               '\u03c3\u003d\u0073\u002c \u017f\u003d\u0073\u002c \ua729\u003d\u0074\u007a\u002c '
                               '\u1efd\u003d\u0076\u002c \u2c73\u003d\u0077\u002c \u0292\u003d\u007a; '
                               + abbreviation_string,
                               styles['Junicode'])

        canvas.restoreState()

        self.__header_footer(canvas, doc)

    def __get_letter_image(self, image_src, height):
        image = pilimage.open('./src/assets/' + image_src)
        img_width, img_height = image.size

        pdf_image_height = height
        pdf_image_width = pdf_image_height * img_width / img_height
        return Image('./src/assets/' + image_src, pdf_image_width, pdf_image_height)

    def __represents_int(self, string):
        try:
            int(string)
            return True
        except ValueError:
            return False
