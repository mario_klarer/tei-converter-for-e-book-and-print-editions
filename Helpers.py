import xml.etree.ElementTree as elTree
import glob
import os

class Helpers:
    @staticmethod
    def get_namespace():
        return 'http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15'

    @staticmethod
    def parse_options(region):
        parse = region.get('custom').strip()

        attributes = {}
        attr = ""
        current_opt = ""
        current_opt_value = ""
        opt = {}
        is_attribute_name = True
        is_opt_name = True

        for char in parse:
            if char != ' ':
                if is_attribute_name:
                    if char != '{':
                        attr = attr + char
                    else:
                        is_attribute_name = False
                else:
                    if is_opt_name:
                        if char == '}':
                            is_attribute_name = True

                            if attr in attributes:
                                attributes[attr].append(opt)
                            else:
                                attributes[attr] = []
                                attributes[attr].append(opt)

                            attr = ""
                            opt = {}
                        elif char != ':':
                            current_opt = current_opt + char
                        else:
                            is_opt_name = False
                    else:
                        if char != ';':
                            current_opt_value = current_opt_value + char
                        else:
                            opt[current_opt] = current_opt_value
                            is_opt_name = True
                            current_opt = ""
                            current_opt_value = ""
        return attributes

    @staticmethod
    def parse_coordinates(coordstring):
        coords = coordstring.split(" ")

        min_x = min_y = 999999
        max_x = max_y = 0

        for coord in coords:
            temp = coord.split(",")
            x_val = int(temp[0])
            y_val = int(temp[1])
            if x_val < min_x:
                min_x = x_val
            if x_val > max_x:
                max_x = x_val
            if y_val < min_y:
                min_y = y_val
            if y_val > max_y:
                max_y = y_val

        return {"max_x": max_x, "max_y": max_y, "min_x": min_x, "min_y": min_y}

    @staticmethod
    def parse_baseline(coordstring):
        coords = coordstring.split(" ")

        min_x = min_y = 999999
        max_x = max_y = 0

        for coord in coords:
            temp = coord.split(",")
            x_val = int(temp[0])
            y_val = int(temp[1])
            if x_val < min_x:
                min_x = x_val
                min_y = y_val
            if x_val > max_x:
                max_x = x_val
                max_y = y_val

        return {"max_x": max_x, "max_y": max_y, "min_x": min_x, "min_y": min_y}

    @staticmethod
    def create_coordstring(coordinates, multiplier, offset, printcoordinates = False):
        coordinates['min_x'] = int(coordinates['min_x'] * multiplier - offset[0])
        coordinates['min_y'] = int(coordinates['min_y'] * multiplier - offset[1])
        coordinates['max_x'] = int(coordinates['max_x'] * multiplier - offset[0])
        coordinates['max_y'] = int(coordinates['max_y'] * multiplier - offset[1])

        coordstring = str(coordinates['min_x']) + "," + str(coordinates['min_y']) + " " + str(
            coordinates['min_x']) + "," + str(coordinates['max_y']) + " " + str(coordinates['max_x']) + "," + str(
            coordinates['min_y']) + " " + str(coordinates['max_x']) + "," + str(
            coordinates['max_y'])
        return coordstring

    @staticmethod
    def represents_int(string):
        try:
            int(string)
            return True
        except ValueError:
            return False

    @staticmethod
    def mm_to_pts(length):
        return length * 72 / 25.4

    @staticmethod
    def split_list(a_list, splitter=None):
        if splitter is None:
            half = len(a_list) // 2
        else:
            half = splitter
        return a_list[:half], a_list[half:]

    @staticmethod
    def get_high_number_unicode(number):
        number_unicodes = ["\u2070", "\u00b9", "\u00b2", "\u00b3", "\u2074", "\u2075", "\u2076", "\u2077", "\u2078",
                           "\u2079"]

        temp_str = str(number)
        ret = ""
        for digit in temp_str:
            ret += number_unicodes[int(digit)]

        return ret

    @staticmethod
    def normalize_string(string):
        to_replace = ["∂", "ɧ", "ɱ", "ŋ", "ꝛ", "ɞ", "σ", "ſ", "ꜩ", "ʒ", "", "ỽ", "ⱳ"]
        replacements = ["d", "h", "m", "n", "r", "s", "s", "s", "tz", "z", "s", "v", "w"]

        ret_string = string
        for index, replacement in enumerate(replacements):
            ret_string = ret_string.replace(to_replace[index], replacement)

        return ret_string

    @staticmethod
    def replace_hyphenations(string, lines):
        for line in lines:
            options = line.get_options()
            junicode = line.get_junicode()

            if 'hyphenation' in options:
                for hyphen in options['hyphenation']:
                    if 'offset' in hyphen and 'length' in hyphen:
                        offset = int(hyphen['offset'])
                        length = int(hyphen['length'])
                        if offset == 0:
                            substring = junicode[offset:(offset + length)]
                            string = string.replace(substring, "%" + substring)
                        else:
                            substring = junicode[offset:(offset + length)]
                            string = string.replace(substring, substring + "%")

        return string

    @staticmethod
    def replace_punctures(string):
        string = string.replace(' ϴ ', '    ')
        string = string.replace('ϴ ', '    ')
        string = string.replace(' ϴ', '    ')
        string = string.replace('ϴ', '    ')

        to_replace = [" ·", " :", " ~", "%  %", "% %", " ", "⸗", "∧"]

        for replace in to_replace:
            string = string.replace(replace, '')

        return string

    @staticmethod
    def replace_logical_and(string, lines):
        for line in lines:
            for insertion in line.get_insertions():
                string = string.replace("∧", insertion, 1)

        return string

    @staticmethod
    def replace_abbreviations(string, lines, replace_unicode=False):
        for line in lines:
            for abbreviation in line.get_abbreviations():
                expansion = abbreviation['expansion']
                if replace_unicode and "cetera" in expansion:
                    expansion = bytes(expansion, 'utf-8').decode('unicode-escape')
                string = string.replace(abbreviation['abbreviated'], expansion, 1)

        return string

    @staticmethod
    def remove_strikethrough(string, lines):
        for line in lines:
            for strike in line.get_strikethroughs():
                string = string.replace(strike.replace('·', 'ϴ'), '', 1)

        return string

    @staticmethod
    def replace_superscripts_capital_letters(string):
        capital_letters = Helpers.get_capital_letter_array()
        bold_capital_letters = Helpers.get_capital_letter_replacement_array()
        red_capital_letters = Helpers.get_capital_letter_replacement_red_array()
        blue_capital_letters = Helpers.get_capital_letter_replacement_blue_array()
        thin_capital_letters = ['ỽ', ]
        thin_small_letters = ['e', 'y', 'v', 'a']
        thick_small_letters = ['w', 'm', ]
        to_replace = ['̄', 'ᷓ', 'ͦ', '̈', '̆', ]
        capital_letter_replacements = ['ϵ', '϶', 'Ϸ', 'ϸ', 'Ϲ']
        thin_small_letter_replacements = ['Ϟ', 'ϟ', 'Ϡ', 'ϡ', 'ϝ']
        thick_small_letter_replacements = ['ϰ', 'ϱ', 'ϲ', 'ϳ', 'ϯ']
        thin_capital_letter_replacements = ['Ц', 'Ч', 'Ш', 'Щ', 'Ъ']

        for letter in capital_letters:
            for i, replacement in enumerate(capital_letter_replacements):
                string = string.replace(letter + to_replace[i], letter + replacement)
        for letter in bold_capital_letters:
            for i, replacement in enumerate(capital_letter_replacements):
                string = string.replace(letter + to_replace[i], letter + replacement)
        for letter in red_capital_letters:
            for i, replacement in enumerate(capital_letter_replacements):
                string = string.replace(letter + to_replace[i], letter + replacement)
        for letter in blue_capital_letters:
            for i, replacement in enumerate(capital_letter_replacements):
                string = string.replace(letter + to_replace[i], letter + replacement)

        for letter in thin_small_letters:
            for i, replacement in enumerate(thin_small_letter_replacements):
                string = string.replace(letter + to_replace[i], letter + replacement)

        for letter in thick_small_letters:
            for i, replacement in enumerate(thick_small_letter_replacements):
                string = string.replace(letter + to_replace[i], letter + replacement)

        for letter in thin_capital_letters:
            for i, replacement in enumerate(thin_capital_letter_replacements):
                string = string.replace(letter + to_replace[i], letter + replacement)

        return string

    @staticmethod
    def replace_superscripts_quotable(string):
        string = string.replace("ͦ", "o")  # Superskriptium o → ‹o›

        string = string.replace("̆", "")  # Breve wird getilgt

        string = string.replace("ÿ", "y")  # Trema über ‹y› wird getilgt
        string = string.replace("Ÿ", "Y")  # Trema über ‹Y› wird getilgt
        string = string.replace("Yᷓ", "Y")  # u über ‹Y› wird getilgt
        string = string.replace("Ѣᷓ", "Ѣϸ")  # u über ‹U› wird getilgt
        string = string.replace("ä", "ä")  # ‹a› mit Trema → Umlaut ‹ä›
        string = string.replace("Ä", "Ä")  # ‹A› mit Trema → Umlaut ‹Ä›
        string = string.replace("ö", "ö")  # ‹o› mit Trema → Umlaut ‹ö›
        string = string.replace("Ö", "Ö")  # ‹O› mit Trema → Umlaut ‹Ö›
        string = string.replace("ü", "ü")  # ‹u› mit Trema → Umlaut ‹ü›
        string = string.replace("Ü", "Ü")  # ‹U› mit Trema → Umlaut ‹Ü›
        string = string.replace("ë", "e")  # ë mit e
        string = string.replace("Ë", "E")  # Ë mit E eϡ

        string = string.replace("yᷓ", "y")  # Superskriptum a über ‹y› wird getilgt
        string = string.replace("yᷓ", "Y")  # Superskriptum a über ‹Y› wird getilgt
        string = string.replace("aᷓ", "ä")  # ‹a› mit Superskriptum a → Umlaut ‹ä›
        string = string.replace("Aᷓ", "Ä")  # ‹A› mit Superskriptum a → Umlaut ‹Ä›
        string = string.replace("eᷓ", "e")  # Superskriptum a über ‹e› wird getilgt
        string = string.replace("Eᷓ", "E")  # Superskriptum a über ‹E› wird getilgt
        string = string.replace("oᷓ", "ö")  # ‹o› mit Superskriptum a → Umlaut ‹ö›
        string = string.replace("Oᷓ", "Ö")  # ‹O› mit Superskriptum a → Umlaut ‹Ö›
        string = string.replace("euᷓ", "eu")  # Superskriptum a über ‹u› in ‹eu› wird getilgt
        string = string.replace("Euᷓ", "Eu")  # Superskriptum a über ‹u› in ‹Eu› wird getilgt
        string = string.replace("uᷓ", "ü")  # ‹u› mit Superskriptum a → Umlaut ‹ü›
        string = string.replace("Uᷓ", "Ü")  # ‹U› mit Superskriptum a → Umlaut ‹Ü›
        string = string.replace("vᷓ", "v̈")  # ‹v› mit Superskriptum a → ‹v› mit Trema
        string = string.replace("Vᷓ", "V̈")  # ‹V› mit Superskriptum a → ‹V› mit Trema
        string = string.replace("wᷓ", "w")  # Superskriptum a über ‹w› wird getilgt
        string = string.replace("Wᷓ", "w")  # Superskriptum a über ‹W› wird getilgt

        string = string.replace("̄", "")  # Makron wird getilgt

        string = string.replace("ˀ", "")  # Glottalstop wird getilgt

        return string

    @staticmethod
    def post_process_verse(verse, lines, indent_initials=False, replace_unicode=False):
        verse = Helpers.replace_punctures(
            Helpers.replace_logical_and(Helpers.normalize_string(
                Helpers.replace_abbreviations(
                    Helpers.remove_strikethrough(Helpers.replace_hyphenations(verse, lines), lines), lines, replace_unicode)), lines))

        if indent_initials is True:
            return Helpers.indent_biginitials(verse)

        return verse

    @staticmethod
    def replace_helper_chars(string):
        string = string.replace("%", "")
        return string

    @staticmethod
    def replace_spaces_with_entities_chars(string):
        string = string.replace("    ", "&nbsp;&nbsp;&nbsp;&nbsp;")
        return string

    @staticmethod
    def merge_two_dicts(x, y):
        z = x.copy()
        z.update(y)
        return z

    @staticmethod
    def indent_biginitials(string):
        capital_letter_replacements = Helpers.get_capital_letter_replacement_array()
        capital_letter_replacements_red = Helpers.get_capital_letter_replacement_red_array()
        capital_letter_replacements_blue = Helpers.get_capital_letter_replacement_blue_array()

        for letter in capital_letter_replacements:
            string = string.replace(letter, "    " + letter)

        for letter in capital_letter_replacements_red:
            string = string.replace(letter, "    " + letter)

        for letter in capital_letter_replacements_blue:
            string = string.replace(letter, "    " + letter)

        return string

    @staticmethod
    def replace_bold_starters(string, styles=True, cmyk=False):
        capital_letters = Helpers.get_capital_letter_array()
        capital_letter_replacements = Helpers.get_capital_letter_replacement_array()
        capital_letter_replacements_red = Helpers.get_capital_letter_replacement_red_array()
        capital_letter_replacements_blue = Helpers.get_capital_letter_replacement_blue_array()

        for index, letter in enumerate(capital_letters):
            if cmyk is True:
                string = string.replace(capital_letter_replacements[index], "<font face='JunicodeBd'>" + letter + "</font>" if styles is True else letter)
                string = string.replace(capital_letter_replacements_red[index], "<font color='CMYKColor(0,1,0.5,0)' face='JunicodeBd'>" + letter + "</font>" if styles is True else letter)
                string = string.replace(capital_letter_replacements_blue[index], "<font color='CMYKColor(1,0,0,0)' face='JunicodeBd'>" + letter + "</font>" if styles is True else letter)
            else:
                string = string.replace(capital_letter_replacements[index], "<font face='JunicodeBd'>" + letter + "</font>" if styles is True else letter)
                string = string.replace(capital_letter_replacements_red[index], "<font color='red' face='JunicodeBd'>" + letter + "</font>" if styles is True else letter)
                string = string.replace(capital_letter_replacements_blue[index], "<font color='deepskyblue' face='JunicodeBd'>" + letter + "</font>" if styles is True else letter)

        return string

    @staticmethod
    def replace_word_with_colored_word(string, word, color, first=True, bold=True):
        font = 'JunicodeBd' if bold is True else 'Junicode'

        if first is True:
            first = word[0]
            rest = word[1:]

            ret = string.replace(word, "<font color='CMYKColor(" + color + ")' face='" + font + "'>" + first + "</font>" + rest)
        else:
            ret = string.replace(word, "<font color='CMYKColor(" + color + ")' face='" + font + "'>" + word + "</font>")

        return ret

    @staticmethod
    def set_thousand_spaces(string):
        if 'u0020' in string:
            return string

        count_of_numbers = 0
        length = len(string)
        insert_space_before = []
        for i in range(length):
            character = string [length - (i + 1)]
            if Helpers.represents_int(character):
                count_of_numbers += 1
            else:
                count_of_numbers = 0

            if count_of_numbers == 3:
                count_of_numbers = 0
                insert_space_before.append(length - (i + 1))

        for insertion in insert_space_before:
            try:
                if "u0020" in string:
                    continue
            except:
                print('Insertion failed')
            string = string[:insertion] + ' ' + string[insertion:]

        return string



    @staticmethod
    def get_capital_letter_array():
        return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                'V', 'W', 'X', 'Y', 'Z', ]

    @staticmethod
    def get_capital_letter_replacement_array():
        return ['г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч',
                'щ', 'ъ', 'ы', 'ь', 'э', ]

    @staticmethod
    def get_capital_letter_replacement_red_array():
        return ['ю', 'я', 'ѐ', 'ё', 'ђ', 'ѓ', 'є', 'ѕ', 'і', 'ї', 'ј', 'љ', 'њ', 'ћ', 'ќ', 'ѝ', 'ў', 'џ', 'Ѡ', 'ѡ', 'Ѣ',
                'ѣ', 'Ѥ', 'ѥ', 'Ѧ', 'ѧ', ]

    @staticmethod
    def get_capital_letter_replacement_blue_array():
        return ['Ҋ', 'ҋ', 'Ҍ', 'ҍ', 'Ҏ', 'ҏ', 'Ґ', 'ґ', 'Ғ', 'ғ', 'Ҕ', 'ҕ', 'Җ', 'җ', 'Ҙ', 'ҙ', 'Қ', 'қ', 'Ҝ', 'ҝ', 'Ҟ',
                'ҟ', 'Ԁ', 'ԁ', 'Ԃ', 'Ԅ', ]
