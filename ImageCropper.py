import glob
import xml.etree.ElementTree as elTree
from Helpers import Helpers
from Model.Textregion import Textregion
from functools import reduce
from PIL import Image, ImageEnhance
import csv
import os, sys
import json
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter('ignore', Image.DecompressionBombWarning)


class ImageCropper:
    min_lines_first_image = 34
    image_width = 1250  # 1250
    image_drawing_addition = 650
    image_drawing_x_offset = 0
    image_height = 2600  # 2375
    coordinate_multiplier = 1
    start_reading_order_at = 1
    standard_offset = 215

    already_connected_images = []

    image_brightness = 0.55  # lower to make brighter
    image_contrast = 1.8  # increase to improve contrast

    def __init__(self, folder, column_title="TEI", csv_file='', offset_file='', merge_file=''):
        self.xmlfiles = glob.glob('./' + folder + '/page/*.xml')
        self.imgfiles = glob.glob('./' + folder + '/*.jpg')
        self.xmlfiles = sorted(self.xmlfiles)
        self.imgfiles = sorted(self.imgfiles)
        self.csv_file = csv_file
        self.csv_data = {}
        self.offset_file = offset_file
        self.offset_data = []
        self.merge_file = merge_file
        self.merge_data = []
        self.column_title = column_title

        self.output_img_directory = "output_cropper/" + self.column_title + "/img"
        self.output_xml_directory = "output_cropper/" + self.column_title + "/xml"
        self.output_temp_directory = "output_cropper/" + self.column_title + "/temp"

        self.folios = {}
        self.illustrations = {}

        if offset_file != '':
            first = True
            with open(offset_file) as offset_file_buffer:
                csv_reader = csv.reader(offset_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    offset_lines = list(map(lambda x: int(x), row[3].split(',')))
                    lines_insert = list(map(lambda x: int(x), row[4].split(',')))

                    if lines_insert[0] == 0:
                        offset_first = offset_lines[0]
                    else:
                        offset_first = 0

                    append = {'file': row[0], 'y': int(row[1]), 'x': int(row[2]), 'lines': offset_first}
                    self.offset_data.append(append)

        if merge_file != '':
            first = True
            with open(merge_file) as merge_file_buffer:
                csv_reader = csv.reader(merge_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    append = {'file': row[0], 'prefix':row[1], 'column_title':row[2]}
                    self.merge_data.append(append)

        if csv_file != '':
            first = True
            with open(csv_file) as csv_file_buffer:
                csv_reader = csv.reader(csv_file_buffer, delimiter=';')
                for row in csv_reader:
                    if first is True:
                        first = False
                        continue

                    append = {'image': row[0], 'x': row[1], 'y': row[2], 'width': row[3], 'height': row[4], 'caption': row[5], 'textregion': row[6], 'linebeginning': row[7], 'lineending': row[8], 'fol': row[9].split(';'), 'connect': row[10], 'offset': int(row[11]), }
                    try:
                        self.csv_data[row[0]].append(append)
                    except:
                        self.csv_data[row[0]] = [append]


        with open('./src/data/liste_captions.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            for row in csv_reader:

                self.folios[row[0]] = {"folio": row[1], "chapter": row[2]}

        with open('./src/data/illustrations.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            for row in csv_reader:
                append = {"left_top": row[1] is "1", "left_bottom": row[2] is "1", "right_top": row[3] is "1",
                          "right_bottom": row[4] is "1", }
                self.illustrations[row[0]] = append

    def cropimages(self):
        for index, xmlfile in enumerate(self.xmlfiles):
            print("Cutting " + xmlfile)
            try:
                imgfile = ""
                for file in self.imgfiles:
                    if os.path.splitext(os.path.basename(file))[0] == os.path.splitext(os.path.basename(xmlfile))[0]:
                        imgfile = file

                if imgfile == "":
                    print("Cropper: No match found" + xmlfile)
                    continue

                if self.csv_file != '' and os.path.basename(imgfile) in self.csv_data:
                    imgfilename = os.path.basename(imgfile)

                    for csv_row_index, csv_row in enumerate(self.csv_data[imgfilename]):
                        xmlroot = elTree.parse(xmlfile).getroot()
                        ns = {'d': Helpers.get_namespace()}

                        page = xmlroot.find('d:Page', ns)

                        regions = page.findall('d:TextRegion', ns)
                        for ind, region in enumerate(regions):
                            textregion = Textregion(region, True)
                            textregion_reading_order = int(textregion.get_options()['readingOrder'][0]['index'])
                            if textregion_reading_order == int(csv_row['textregion']) - 1:
                                lines = textregion.get_lines_by_reading_order(int(csv_row['linebeginning']) - 1, int(csv_row['lineending']) - 1)

                                if csv_row['connect'] != '':
                                    if csv_row['connect'] in self.already_connected_images:
                                        continue

                                    self.already_connected_images.append(csv_row['connect'])

                                    connectxmlfiles = [xmlfile]
                                    connectimgfiles = [imgfile]
                                    connectimgfiles_rendered = []
                                    connect_csv_rows = [csv_row]
                                    connectimgpixels = []
                                    connectlines = [lines]
                                    connecttextregions = [textregion]

                                    for csv_row_connect_index, csv_row_connect in enumerate(self.csv_data[imgfilename]):
                                        if csv_row_connect != '' and csv_row['connect'] == csv_row_connect['connect'] and csv_row_connect_index != csv_row_index:
                                            xml_file_connect = ''
                                            for file in self.xmlfiles:
                                                if os.path.splitext(os.path.basename(file))[0] == \
                                                        os.path.splitext(os.path.basename(csv_row_connect['image']))[0]:
                                                    xml_file_connect = file

                                            if xml_file_connect == '':
                                                continue

                                            img_file_connect = ''
                                            for file in self.imgfiles:
                                                if os.path.splitext(os.path.basename(file))[0] == \
                                                        os.path.splitext(os.path.basename(csv_row_connect['image']))[0]:
                                                    img_file_connect = file

                                            if img_file_connect == '':
                                                continue

                                            connectxmlfiles.append(xml_file_connect)
                                            connectimgfiles.append(img_file_connect)
                                            connect_csv_rows.append(csv_row_connect)

                                            xmlroot_connect = elTree.parse(xml_file_connect).getroot()

                                            page_connect = xmlroot_connect.find('d:Page', ns)

                                            regions_connect = page_connect.findall('d:TextRegion', ns)

                                            for ind, region_connect in enumerate(regions_connect):
                                                textregion_connect = Textregion(region_connect, True)
                                                textregion_reading_order_connect = int(
                                                    textregion_connect.get_options()['readingOrder'][0]['index'])
                                                if textregion_reading_order_connect == int(csv_row_connect['textregion']) - 1:
                                                    lines_connect = textregion_connect.get_lines_by_reading_order(
                                                        int(csv_row_connect['linebeginning']) - 1,
                                                        int(csv_row_connect['lineending']) - 1)
                                                    connectlines.append(lines_connect)
                                                    connecttextregions.append(textregion_connect)
                                        else:
                                            continue

                                    for conn_ind, connectimgfile in enumerate(connectimgfiles):
                                        image_pixels, connectimgfilepath = self.__crop_by_csv(connect_csv_rows[conn_ind], connectimgfile, '', '_temp', True)

                                        connectimgpixels.append(image_pixels)
                                        connectimgfiles_rendered.append(connectimgfilepath)


                                    new_connected_image = Image.new('CMYK', (self.image_width + self.image_drawing_addition, self.image_height), (0, 0, 0, 0))

                                    distance_between_images = self.image_height

                                    for connect_img_pixels in connectimgpixels:
                                        distance_between_images -= (connect_img_pixels[3] - connect_img_pixels[1])

                                    connect_current_offset = 0
                                    for rendered_index, rendered_connect_imgfile in enumerate(connectimgfiles_rendered):
                                        img_temp = Image.open(rendered_connect_imgfile)

                                        new_connected_image.paste(img_temp, (0, connect_current_offset))

                                        connect_current_offset += (connectimgpixels[rendered_index][3] - connectimgpixels[rendered_index][1]) + distance_between_images

                                    if not os.path.exists(self.output_img_directory):
                                        os.makedirs(self.output_img_directory)

                                    new_connected_image.save(self.output_img_directory + "/" + connect_csv_rows[1]['caption'] + '.jpg', 'JPEG')

                                    self.__create_xml_multiple(connectlines, connect_csv_rows[1]['caption'] + '.xml', connectimgpixels,
                                                          connecttextregions, connect_csv_rows, distance_between_images)

                                else:
                                    image_pixels, test = self.__crop_by_csv(csv_row, imgfile, '', '',)

                                    self.__create_xml(lines, csv_row['caption'] + '.xml', '', '', image_pixels, csv_row['offset'], textregion,
                                                  False, False, csv_row, True)

                    continue

                xmlroot = elTree.parse(xmlfile).getroot()
                ns = {'d': Helpers.get_namespace()}

                page = xmlroot.find('d:Page', ns)

                regions = page.findall('d:TextRegion', ns)
                for ind, region in enumerate(regions):
                    textregion = Textregion(region, True)
                    textregion_reading_order = int(textregion.get_options()['readingOrder'][0]['index'])
                    is_drawing_left_top = textregion_reading_order == 0 and \
                                          self.illustrations[os.path.basename(xmlfile)]['left_top']
                    is_drawing_left_bottom = textregion_reading_order == 0 and \
                                             self.illustrations[os.path.basename(xmlfile)]['left_bottom']
                    is_drawing_right_top = textregion_reading_order == 2 and \
                                           self.illustrations[os.path.basename(xmlfile)]['right_top']
                    is_drawing_right_bottom = textregion_reading_order == 2 and \
                                              self.illustrations[os.path.basename(xmlfile)]['right_bottom']

                    if textregion_reading_order > 2:
                        continue
                    lines = textregion.get_lines()

                    lines_amount = len(lines)

                    if lines_amount > 0:
                        if textregion_reading_order == 0:
                            prefix = "a"
                        elif textregion_reading_order == 1:
                            prefix = "b"
                        else:
                            prefix = "c"

                        if len(lines) > self.min_lines_first_image:
                            if len(lines) / self.min_lines_first_image > 2.0:
                                splitter = len(lines) - self.min_lines_first_image
                            else:
                                splitter = self.min_lines_first_image

                            lines_a, lines_b = Helpers.split_list(lines, splitter)

                            last_two_of_lines_a = lines_a[-2:]

                            # Move Lines on cases Start

                            # Move Lines from a to b if big initial is in last two lines
                            for j, line_a in enumerate(last_two_of_lines_a):
                                if "bigInitial" in line_a.get_options():
                                    lines_b = lines_a[-(2-j):] + lines_b
                                    lines_a = lines_a[:-(2-j)]
                                    break

                            # Move Lines from a to b if incipit appears in last lines of a and first line of b
                            if "incipit" in lines_b[0].get_options() and "incipit" in lines_a[-1].get_options():
                                incipit_offset = 0
                                for line_a in reversed(lines_a):
                                    if "incipit" in line_a.get_options():
                                        incipit_offset+=1

                                lines_b = lines_a[-1 * incipit_offset:] + lines_b
                                lines_a = lines_a[:-1 * incipit_offset]

                            # Move Lines on cases End

                            # Merge Files

                            merge_b = False
                            for merge in self.merge_data:
                                if merge['file'] in imgfile and prefix == merge['prefix'] and self.column_title == merge['column_title']:
                                    merge_b = True
                                    break

                            if merge_b:
                                lines_a = lines_a + list(filter(lambda x: x.get_junicode() != "", lines_b))

                            # Merge Files End

                            suffix_a, empty_lines_offset, empty_lines_until_last_offset, last_line = ImageCropper.get_suffix(lines_a, textregion.get_left_offset())

                            if merge_b is False:
                                suffix_b, ofs, ofs_last, l_line = ImageCropper.get_suffix(lines_b, last_line)

                            if self.only_consists_of_empty_lines(lines_a) is False:
                                image_pixels_a = self.__crop_lines(lines_a, imgfile, prefix, suffix_a, is_drawing_left_top,
                                                                   is_drawing_right_top)

                                self.__create_xml(lines_a, xmlfile, prefix, suffix_a, image_pixels_a, textregion.get_left_offset(), textregion,
                                                  is_drawing_left_top, is_drawing_right_top)

                            if merge_b is False and self.only_consists_of_empty_lines(lines_b) is False:
                                image_pixels_b = self.__crop_lines(lines_b, imgfile, prefix, suffix_b,
                                                                   is_drawing_left_bottom,
                                                                   is_drawing_right_bottom)
                                self.__create_xml(lines_b, xmlfile, prefix, suffix_b, image_pixels_b,
                                                      len(lines_a) - empty_lines_until_last_offset + textregion.get_left_offset(), textregion, is_drawing_left_bottom,
                                                      is_drawing_right_bottom)
                        else:
                            suffix_a, ofs, ofs_last, last_line = ImageCropper.get_suffix(lines, textregion.get_left_offset())
                            image_pixels = self.__crop_lines(lines, imgfile, prefix, suffix_a, is_drawing_left_top,
                                                             is_drawing_right_top)
                            self.__create_xml(lines, xmlfile, prefix, suffix_a, image_pixels, textregion.get_left_offset(), textregion,
                                              is_drawing_left_top, is_drawing_right_top)
            except IndexError:
                continue
            except IOError:
                continue

    def __crop_by_csv(self, row, imgfile, prefix, suffix, to_temp_folder = False):
        min_x = int(row['x'])
        min_y = int(row['y'])
        max_x = int(row['x']) + int(row['width'])
        max_y = int(row['y']) + int(row['height'])

        image_crop_dimensions = (min_x, min_y, max_x, max_y)

        img = Image.open(imgfile)
        cropped_img = img.crop(image_crop_dimensions)

        folioname = row['caption']

        if not os.path.exists(self.output_temp_directory):
            os.makedirs(self.output_temp_directory)

        filepath = self.output_temp_directory + "/" + prefix + folioname + suffix + os.path.splitext(os.path.basename(imgfile))[1]

        if to_temp_folder is False:
            if not os.path.exists(self.output_img_directory):
                os.makedirs(self.output_img_directory)

            filepath = self.output_img_directory + "/" + folioname + os.path.splitext(os.path.basename(imgfile))[1]

        cropped_img.save(
            filepath,
            format='JPEG', subsampling=0, quality=100)

        return image_crop_dimensions, filepath

    def __crop_lines(self, lines, imgfile, prefix, suffix, is_drawing_left=False, is_drawing_right=False):
        coords = {}

        coords["min_y"] = 999999
        coords["max_y"] = -1
        coords["min_x"] = 999999
        coords["max_x"] = -1

        for line in lines:
            bl = line.get_baseline()

            if coords["min_y"] > bl["min_y"]:
                coords["min_y"] = bl["min_y"]
            if coords["max_y"] < bl["max_y"]:
                coords["max_y"] = bl["max_y"]
            if coords["min_x"] > bl["min_x"]:
                coords["min_x"] = bl["min_x"]
            if coords["max_x"] < bl["max_x"]:
                coords["max_x"] = bl["max_x"]

        center_x = (coords["max_x"] * self.coordinate_multiplier + coords["min_x"] * self.coordinate_multiplier) // 2
        min_x = center_x - self.image_width // 2 - (self.image_drawing_addition if is_drawing_left is True else 0) \
                + (self.image_drawing_x_offset if is_drawing_right is True else 0)
        if min_x < 0:
            max_x_add = abs(min_x)
            min_x = 0
        else:
            max_x_add = 0

        min_y = coords["min_y"] * self.coordinate_multiplier - self.standard_offset

        if min_y < 0:
            min_y = 0

        max_x = center_x + self.image_width // 2 + (self.image_drawing_addition if is_drawing_right is True else 0) \
                - (self.image_drawing_x_offset if is_drawing_left is True else 0) + max_x_add
        max_y = min_y + self.image_height

        try:
            folioname = self.folios[os.path.basename(imgfile)]['folio']
        except (IndexError, KeyError):
            folioname = os.path.splitext(os.path.basename(imgfile))[0]

        filename = folioname + prefix + " " + suffix + os.path.splitext(os.path.basename(imgfile))[1]

        for offset_row in self.offset_data:
            if filename == offset_row['file']:
                if offset_row['x'] != 0:
                    min_x = offset_row['x']
                    max_x = offset_row['x'] + self.image_width
                if offset_row['y'] != 0:
                    min_y = offset_row['y']
                    max_y = offset_row['y'] + self.image_height

        image_crop_dimensions = (min_x, min_y, max_x, max_y)

        img = Image.open(imgfile)
        cropped_img = img.crop(image_crop_dimensions)

        if not os.path.exists(self.output_img_directory):
            os.makedirs(self.output_img_directory)

        cropped_img.save(
            self.output_img_directory + "/" + filename,
            format='JPEG', subsampling=0, quality=100)

        return image_crop_dimensions

    def __create_xml_multiple(self, lines_array, filename, image_pixels_array, regions_array, csv_row_array, y_offset_addition):
        root = elTree.Element('PcGts')
        root.set('xmlns', Helpers.get_namespace())
        page = elTree.SubElement(root, 'Page')
        textregion = elTree.SubElement(page, 'TextRegion')

        textregion_options = {}

        for region in regions_array:
            textregion_options = Helpers.merge_two_dicts(textregion_options, region.get_options())

        if "drawing" in textregion_options:
            del textregion_options['drawing']

        textregion_options['drawing'] = [{'left': 0}]
        textregion.set('custom', json.dumps(textregion_options))

        chaptername = "Tabula"
        textregion.set('chapter', chaptername)

        y_offset = 0
        skipped = 0
        last_lines_offset = 0

        for lines_ind, lines in enumerate(lines_array):
            image_height = image_pixels_array[lines_ind][3] - image_pixels_array[lines_ind][1]

            image_pixels_array[lines_ind] = (image_pixels_array[lines_ind][0], image_pixels_array[lines_ind][1] - y_offset, image_pixels_array[lines_ind][2],image_pixels_array[lines_ind][3] - y_offset)

            y_offset += image_height + y_offset_addition

            for index, line in enumerate(lines):
                if str(int(line.get_reading_order()) + 1) in csv_row_array[lines_ind]['fol']:
                    is_folio = True
                else:
                    is_folio = False

                xml_line = elTree.SubElement(textregion, 'TextLine')
                coords = elTree.SubElement(xml_line, 'Coords')
                coordinates = line.get_coords()
                coordstring = Helpers.create_coordstring(coordinates, self.coordinate_multiplier, image_pixels_array[lines_ind])
                coords.set('points', coordstring)
                baseline = line.get_baseline()
                bline = elTree.SubElement(xml_line, 'Baseline')
                baselinestring = Helpers.create_coordstring(baseline, self.coordinate_multiplier, image_pixels_array[lines_ind])
                bline.set('points', baselinestring)
                junicode = elTree.SubElement(elTree.SubElement(xml_line, 'TextEquiv'), 'Unicode')
                junicode.text = line.get_junicode()
                opts = line.get_options()
                if is_folio:
                    opts['folio'] = [{'is_folio': 'true'}]
                else:
                    opts['folio'] = [{'is_folio': 'false'}]
                xml_line.set('custom', json.dumps(opts))
                xml_line.set('readingOrder', str(index - skipped + last_lines_offset + self.start_reading_order_at))

            last_lines_offset += len(lines)

        tree = elTree.ElementTree(root)

        if not os.path.exists(self.output_xml_directory):
            os.makedirs(self.output_xml_directory)

        tree.write(
            self.output_xml_directory + '/' + filename)

    def __create_xml(self, lines, filename, prefix, suffix, image_pixels, offset, region, is_drawing_left,
                     is_drawing_right, csv_row= {}, is_use_filename = False):
        skipped = 0
        root = elTree.Element('PcGts')
        root.set('xmlns', Helpers.get_namespace())
        page = elTree.SubElement(root, 'Page')
        textregion = elTree.SubElement(page, 'TextRegion')
        textregion_options = region.get_options()
        if "drawing" in textregion_options:
            del textregion_options['drawing']

        if is_drawing_left is True or is_drawing_right is True:
            textregion_options['drawing'] = [{'left': 1 if is_drawing_left is True else 0}]
        textregion.set('custom', json.dumps(textregion_options))
        try:
            chaptername = self.folios[os.path.basename(os.path.splitext(os.path.basename(filename))[0] + ".jpg")][
                'chapter']
        except (KeyError, IndexError):
            chaptername = "TEI"
        textregion.set('chapter', chaptername)
        for index, line in enumerate(lines):
            if csv_row != {} and str(int(line.get_reading_order()) + 1) in csv_row['fol']:
                is_folio = True
            else:
                is_folio = False

            xml_line = elTree.SubElement(textregion, 'TextLine')
            coords = elTree.SubElement(xml_line, 'Coords')
            coordinates = line.get_coords()
            coordstring = Helpers.create_coordstring(coordinates, self.coordinate_multiplier, image_pixels)
            coords.set('points', coordstring)
            baseline = line.get_baseline()
            bline = elTree.SubElement(xml_line, 'Baseline')
            baselinestring = Helpers.create_coordstring(baseline, self.coordinate_multiplier, image_pixels)
            bline.set('points', baselinestring)
            junicode = elTree.SubElement(elTree.SubElement(xml_line, 'TextEquiv'), 'Unicode')
            junicode.text = line.get_junicode()
            opts = line.get_options()
            if is_folio:
                opts['folio'] = [{'is_folio': 'true'}]
            else:
                opts['folio'] = [{'is_folio': 'false'}]
            xml_line.set('custom', json.dumps(opts))
            xml_line.set('readingOrder', str(offset + index + self.start_reading_order_at - skipped))

        try:
            folioname = self.folios[os.path.basename(os.path.splitext(os.path.basename(filename))[0] + ".jpg")]['folio']
        except (IndexError, KeyError):
            folioname = os.path.splitext(os.path.basename(filename))[0]

        tree = elTree.ElementTree(root)
        file_name_temp = folioname + prefix + " " + suffix + os.path.splitext(os.path.basename(filename))[1]

        if is_use_filename is True:
            file_name_temp = filename

        if not os.path.exists(self.output_xml_directory):
            os.makedirs(self.output_xml_directory)

        for offset_row in self.offset_data:
            if os.path.splitext(os.path.basename(offset_row['file']))[0] == os.path.splitext(os.path.basename(file_name_temp))[0]:
                textregion.set('rightOffset', str(offset_row['lines']))

        tree.write(
            self.output_xml_directory + "/" + file_name_temp)

    def only_consists_of_empty_lines(self, lines):
        empty = True
        for line in lines:
            if line.get_junicode() != "":
                empty = False
                break

        return empty

    @staticmethod
    def get_added_pixels_y():
        return 20

    @staticmethod
    def get_suffix(lines, offset):
        empty_lines = 0
        empty_lines_unicode = 0
        empty_lines_until_last = 0
        empty_lines_until_last_temp = 0

        if lines is not None and len(lines) > 0:
            for line in lines:
                empty_lines += line.get_empty_lines()
                if line.get_junicode() is None or line.get_junicode() == "":
                    empty_lines_unicode += 1
                    empty_lines_until_last_temp+=1
                else:
                    empty_lines_until_last+=empty_lines_until_last_temp
                    empty_lines_until_last_temp = 0

        first_line = max(1 + offset, 1)
        last_line = max(offset + len(lines) - empty_lines_unicode, 1)

        if first_line == last_line:
            line_return = "l. " + str(first_line)
        else:
            line_return = "ll. " + str(first_line) + "–" + str(last_line)

        return line_return, empty_lines_unicode, empty_lines_until_last, max(offset + len(lines) - empty_lines_unicode, 0)
